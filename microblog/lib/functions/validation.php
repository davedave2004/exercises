<?php

    function checkUsernamePattern($username) {
        $usernamePattern = '/^[0-9a-z@.?!_]+$/i';
        if (!preg_match($usernamePattern, $username)) {
            return false;
        }
        return true;
    }
    function checkEmailPattern($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
    function checkNamePattern($fullname) {
        //$rawFullname = str_replace(" ",'',$fullname)
        //return ctype_alnum($fullname);
        $namePattern = '/^[a-z ]+[a-z]$/i';
        if (!preg_match($namePattern, $fullname)) {
            return false;
        }
        return true;        
    }
    function validateFile($file,$type) {
        $errorMessages = "";
        $pictureMaxSize = 5000000; //5MB
        $validFileTypes = array("jpg","jpeg","png","gif");

        if (!in_array($type, $validFileTypes)) {
            $errorMessages .= "Not a valid picture. ";
        }
        if ($file["size"] > $pictureMaxSize) {
            $errorMessages .= "Image is too big. ";
        }
        return $errorMessages;
    }
    function checkPostLength($content) {
        if (strlen($content)>140) {
            return false;
        }
        return true;
    }

?>