<?php
	include('dbfunctions.php');
	include('validation.php');

	if(isset($_POST['do'])){
		$do = $_POST['do'];

		switch($do):
			case 'follow':{
				$user = $_POST['user'];
				$toFollow = $_POST['follow'];
				$followers = array();
				if(isAlreadyFollowing($mysqliconn,$user,$toFollow)){
					if(!unFollowUser($mysqliconn,$user,$toFollow)){
						echo 'UnFollow Failed';
						return;
						exit;
					}
				}else {
					if(!followUser($mysqliconn,$user,$toFollow)){
						echo 'Follow Failed';
						return;
						exit;
					}
				}
				$followers['toFollow'] = getFollowerCount($mysqliconn,$toFollow);
				$followers['user'] = getFollowingCount($mysqliconn,$user);
				echo json_encode($followers); 
				break;
			}
			case 'like':{
				$user = $_POST['user'];
				$postid = $_POST['post'];
				if(isAlreadyLiked($mysqliconn,$user,$postid)){
					if(!unlikePost($mysqliconn,$user,$postid)){
						exit('Like Failed. Something may be wrong with the post');
					}
				}else {
					if(!likePost($mysqliconn,$user,$postid)){
						exit('Like Failed. Something may be wrong with the post');
					}
				}
				$count = getPostLikeCount($mysqliconn,$postid);
				echo json_encode($count);

				break;
			}
			case 'getpost': {
				$postid = $_POST['post'];
				$postInfo = getPost($mysqliconn,$postid);
				if($postInfo) {
					echo json_encode($postInfo);
				}
				else {
					echo "Could not get post. Post may have been deleted";
				}
				break;
			}
			case 'refreshfeed': {
				$for = $_POST['for'];
				if(isset($_POST['acctid']))	$id = $_POST['acctid'];
				$user = $_POST['user'];
				$page = $_POST['page'];
				if(strcmp($for,'home')==0){
					$postIds = getAllPostIdsFor($mysqliconn,$user,$page);
				}else $postIds = getAllPostIdsBy($mysqliconn,$user,$page);

				$newFeed = array();
				while($post = $postIds->fetch_assoc()){
					$postid = $post['id'];
					$completeInfo = array();

					if($postInfo = getPost($mysqliconn,$postid)){
						$postInfo['like_count'] = getPostLikeCount($mysqliconn,$postInfo['id']);
						$postInfo['repost_count'] = getPostRepostCount($mysqliconn,$postInfo['id']);
						$postInfo['date_created']= formattedDate($postInfo['date_created']);
						if($postInfo['is_repost']){

							$repostedInfo  = getPost($mysqliconn,$postInfo['reposted_post_id']);

							$completeInfo['repost'] = $repostedInfo;
						}

						if(strcmp($for,'home')==0){
							$postInfo['is_liked'] = isAlreadyLiked($mysqliconn,$user,$postid);
						} else $postInfo['is_liked'] = isAlreadyLiked($mysqliconn,$id,$postid);
							
							
						
						$comments = getCommentsForPost($mysqliconn,$postid);
						if($comments) $completeInfo['comments'] = $comments;

						$completeInfo['main'] = $postInfo;
						array_push($newFeed,$completeInfo);
					}
				}
				echo json_encode($newFeed);
				break;
			}
			case 'getfullpost': {
				$postid = $_POST['post'];
				$completeInfo = array();

				if($postInfo = getPost($mysqliconn,$postid)){
					$postInfo['like_count'] = getPostLikeCount($mysqliconn,$postInfo['id']);
					$postInfo['repost_count'] = getPostRepostCount($mysqliconn,$postInfo['id']);
					$postInfo['date_created']= formattedDate($postInfo['date_created']);
					if($postInfo['is_repost']){

						$repostedInfo  = getPost($mysqliconn,$postInfo['reposted_post_id']);

						$completeInfo['repost'] = $repostedInfo;
					}
					if(!empty($_POST['user'])) {
						$user = $_POST['user'];
						$postInfo['is_liked'] = isAlreadyLiked($mysqliconn,$user,$postid);
					}
					$comments = getCommentsForPost($mysqliconn,$postid);
					if($comments) $completeInfo['comments'] = $comments;

					$completeInfo['main'] = $postInfo;
					echo json_encode($completeInfo);					
				}else echo "Could not get post. Post may have been deleted";
				break;
			}			
			case 'repost': {
				$userid = $_POST['user'];
				$postid = $_POST['post'];
				$content = $_POST['content'];
				if(insertPost($mysqliconn,$userid,$content,NULL,1,$postid)){
					$count = getPostRepostCount($mysqliconn,$postid);
					echo json_encode($count);
				}else echo "Could not repost!.";
				break;
			}
			case 'post': {
				$errorMessage = "";
				$userid = $_POST['user'];
				$content = $_POST['content'];
				$pictureFile = $_FILES['attached'];
				$hasPicture = $_POST['hasPicture'];

				if(!checkPostLength($content)) $errorMessage .= "Post is too long. ";

				if($pictureFile['error']==4){
					$picture = NULL;
				}else{
					if(isset($pictureFile)){
						$directory = "../../public_html/images/attached/";
						$fileType = strtolower(pathinfo(basename($pictureFile["name"]),PATHINFO_EXTENSION));
						$errorMessage .= validateFile($pictureFile,$fileType);
						
						if(!$errorMessage){
							$filename = $directory.$userid.date("ymdLHis");
							$filename .= '.'.$fileType;
							if(move_uploaded_file($pictureFile["tmp_name"], "$filename")){
								$completeFilename = explode("/", $filename);
								$picture = $completeFilename[sizeof($completeFilename)-1];
							}else {
								$errorMessage .= "Uploading problem. ";
							}							
						}
					}
				}
				//if this was an edit post or original post
				if($errorMessage) {
					return;
					exit($errorMessage);
				}

				if(isset($_POST['post']) && strlen($_POST['post'])){
					$postid = $_POST['post'];
					if(!editPost($mysqliconn,$postid,$content,$picture,$hasPicture)){
						exit("Could not Edit Post");
					}
				}else {
					//no post id included so original post
					if(!insertPost($mysqliconn,$userid,$content,$picture,0,NULL)){
						exit("Could not post");
					}
				}
				echo json_encode(getUserPostCount($mysqliconn,$userid));
				break;
			}			
			case 'checkuser': {
				$username = $_POST['username'];
				if(checkUsernameIfTaken($mysqliconn,$username)){
					echo 1; //echo 1 if not taken
				}else echo 'taken'; //0 if taken
				break;
			}
			case 'comment': {
				$userid = $_POST['user'];
				$postid = $_POST['postid'];
				$comment = $_POST['comment'];
				if(!insertComment($mysqliconn,$userid,$postid,$comment)){
					exit("Could not comment!");
				}
				$count = getPostCommentCount($mysqliconn,$postid);
				echo json_encode($count);
				break;
			}
			case 'delete':{
				$postid = $_POST['post'];
				if(deletePost($mysqliconn,$postid)){
					echo 1;
					return;
					exit;
				}else echo "Could not delete.";
				break;
			}
			case 'getacctinfo': {
				$user = $_POST['user'];//either id or username
				$type = $_POST['type'];//either id or username
				if($stats = getAcctInfo($mysqliconn,$user,$type)){
					echo json_encode($stats);
				}else echo "Could not refresh the data";

				break;
			}
			case 'getpoststats':{
				$postid = $_POST['post'];
				$stats = array();
				$stats['like_count'] = getPostLikeCount($mysqliconn,$postid); 
				$stats['repost_count'] = getPostRepostCount($mysqliconn,$postid);
				$stats['comment_count'] = getPostCommentCount($mysqliconn,$postid);
				echo json_encode($stats);
				break;
			}
			default: {
				echo "Invalid Request";
				break;
			}
		endswitch;
	}

?>