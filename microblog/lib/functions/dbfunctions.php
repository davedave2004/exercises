<?php
    include("db.php");

    function checkUsernameIsTaken($conn,$username) {
        $select = "SELECT `id` FROM `users` WHERE `username`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("s",$username);
            $stmt->execute();

            $result = $stmt->get_result();
            $rows = $result->num_rows;

            $result->free();
            $stmt->close();

            return $rows==0;
        }else echo "Could not check username. ".$conn->error;
    }

    function verifyPassword($conn,$userid,$password) {
        $select = "SELECT `password` FROM `users` WHERE `id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $pass = $result->fetch_assoc();
        
            $result->free();
            $stmt->close();
            return password_verify($password,$pass['password']);
        }
    }

    function changePassword($conn,$userid,$password) {
        $update = "UPDATE `users` SET `password`=? WHERE `id`=?";
        $hashedpass = password_hash($password,PASSWORD_DEFAULT);

        if ($stmt = $conn->prepare($update)) {
            $stmt->bind_param("si",$hashedpass,$userid);
            $result = $stmt->execute();

            $stmt->close();
            return($result);
        }
    }

    function getAcctInfo($conn,$key,$type='id') {
        //IF id is given, use id, if not use username
        $selectquery = "SELECT `a`.`id`,`email`,`full_name`,`description`,`username`,`profile_pic`,COUNT(`user_id`) as 'post_count' FROM `users` as `a` LEFT JOIN `posts` ON `a`.`id`=`user_id`";
        if (strcmp($type,'id')==0) {
            $selectquery .= "WHERE `a`.`id`=? GROUP BY `a`.`id`;";
        }
        else {
            $selectquery .= "WHERE `username`=? GROUP BY `a`.`id`;";
        }

        if ($preparedStmt = $conn->prepare($selectquery)) {
            //IF id is given, use id, if not use username
            (strcmp($type, 'id')==0) ? $preparedStmt->bind_param('i',$key) : $preparedStmt->bind_param('s',$key);
            $preparedStmt->execute();
            $result = $preparedStmt->get_result();
            $preparedStmt->close();
            $array = $result->fetch_assoc();

            //get follower and following count and repost count and like count

            $array['follower_count'] = getFollowerCount($conn,$array['id']);
            $array['following_count'] = getFollowingCount($conn,$array['id']);
            $array['like_count'] = getProfileLikeCount($conn,$array['id']);
            $array['repost_count'] = getProfileRepostCount($conn,$array['id']);

            return $array;
        }else echo "Could not get info.";
    }   
    
    function register($conn,$userInfo,$token) {
        $message = false;
        $insertquery = "INSERT INTO users(username,password,email,full_name,verification_token) VALUES (?,?,?,?,?);";
        $username = $userInfo['username'];
        $hashedpass = password_hash($userInfo['password'],PASSWORD_DEFAULT);
        if ($prepInsert = $conn->prepare($insertquery)) {
            $prepInsert->bind_param("sssss",$username,$hashedpass,$userInfo['email'],$userInfo['fullname'],$token);
            if ($prepInsert->execute()) {
                $message = "You are now registered!";
            } else echo $conn->error;
        } else echo $prepInsert->error;
        $prepInsert->close();

        return $message;
    }

    function updateProfPic($conn,$id,$picture) {
        $updateQuery = "UPDATE `users` SET `profile_pic`=? WHERE `id`=?;";
        if ($stmt = $conn->prepare($updateQuery)) {
            $stmt->bind_param("si",$picture,$id);
            $result = $stmt->execute();
            $stmt->close();
            return $result;
        }
    }

    function updateProfInfo($conn,$id,$data) {
        $updateQuery = "UPDATE `users` SET `full_name`=?,`description`=? WHERE `id`=?";

        if ($stmt = $conn->prepare($updateQuery)) {
            $stmt->bind_param("ssi",$data['fullname'],$data['description'],$id);
            $result = $stmt->execute();
            $stmt->execute();
            return $result;
        }
        echo "Could not update. ".$conn->error;
    }

    function getLogin($conn,$data) {
        //Check first if username is registered
        if (checkDataIfTaken($conn,$data['username'],"username") || checkDataIfTaken($conn,$data['username'],"email")) {
            //Check if username and password matches the data in db
            $selectquery = "SELECT `id`,`is_verified`,`username`,`password` FROM users WHERE username=? || email=?";
            if ($prepSelect = $conn->prepare("$selectquery")) {
                $prepSelect->bind_param("ss",$data['username'],$data['username']);
                $prepSelect->execute();
                $result = $prepSelect->get_result();
                $prepSelect->close();
                if ($result->num_rows>0) {
                    $row = $result->fetch_assoc();
                    $checkpass = $row['password'];
                    if (password_verify($data['password'],$checkpass)) {
                        array_pop($row);
                        return  $row;
                    } else return "Wrong account or password"; 
                }
            }
        } else return "That user is not registered";
        return false;
    }

    function checkDataIfTaken($conn,$data,$category) {
        $selectquery = "SELECT `$category` FROM users WHERE `$category`=?";

        if ($prepSelect = $conn->prepare($selectquery)) {
            $prepSelect->bind_param('s',$data);
            $prepSelect->execute();
            $result = $prepSelect->get_result();
            $prepSelect->close();
            return ($result->num_rows >0);
        } else echo $conn->error;
    }

    //Check token is in database
    //Update is_verified and return name and id
    //
    function verifyEmail($conn,$token) {
        if (checkDataIfTaken($conn,$token,"verification_token")) {
            $selectInfo = "SELECT id,username,full_name FROM users WHERE verification_token=?";
            if ($stmt = $conn->prepare($selectInfo)) {
                $stmt->bind_param("s",$token);
                $stmt->execute();
                $result = $stmt->get_result();
                $stmt->close();

                if ($result->num_rows >0) {
                    $row = $result->fetch_assoc();
                    $result->free();
                    $updateQuery = "UPDATE `users` SET is_verified=1 WHERE verification_token=?";
                    if ($updateStmt = $conn->prepare($updateQuery)) {
                        $updateStmt->bind_param("s",$token);
                        if ($updateStmt->execute()) {

                            $updateStmt->close();
                            return $row;
                        } 
                    }
                    $row['error']="Could not verify your account at the moment. ".$conn->error;
                    return $row;
                }       
            }
        }
        return "There is something wrong, try clicking the link on your email again"; 
    }

    function getEmailAndToken($conn,$username) {
        $selectquery = "SELECT `email`,`verification_token` FROM users WHERE `username`=?";

        if ($preparedStmt = $conn->prepare($selectquery)) {
            $preparedStmt->bind_param('s',$username);
            $preparedStmt->execute();
            $result = $preparedStmt->get_result();
            $array = $result->fetch_assoc();
            $result->free();
            $preparedStmt->close();
            return $array;
        }       
    }

    //get comments for post
    function getCommentsForPost($conn,$postid) {
        $getComment = "SELECT `b`.`full_name`,`b`.`username`,`b`.`profile_pic`,`a`.`comment`,`a`.`date_created` FROM `comments` as `a` INNER JOIN `users` as `b` ON `a`.`user_id`=`b`.`id` WHERE `post_id`=? ORDER BY `a`.`id` DESC";

        if ($stmt = $conn->prepare($getComment)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $results = $stmt->get_result();
            if ($results->num_rows == 0) {
                $result =  false;
            }else{
                $result = array();
                while ($row = $results->fetch_assoc()) {
                    array_push($result, $row);
                }
            }
            $results->free();
            $stmt->close();
            return $result;
        }
    }

    function insertComment($conn,$userid,$postid,$comment) {
        $insertComment = "INSERT INTO `comments` (`user_id`, `post_id`, `comment`) VALUES (?,?,?);";

        if ($stmt = $conn->prepare($insertComment)) {
            $stmt->bind_param("iis",$userid,$postid,$comment);
            $result = $stmt->execute();

            $stmt->close();
            return $result;
        }else echo "Could not insert comment. ".$conn->error;
        return false;
    }

    //Get all posts by this person
    function getAllPostsBy($conn,$username,$page=0) {
        
        $offset = ($page > 1) ? ($page-1)*20 : 0;

        $selectquery= "SELECT `a`.`id`, `b`.`full_name`,`b`.`username`,`b`.`profile_pic`, `a`.`content`, `a`.`picture`, `a`.`date_created`, `a`.`date_updated`, `a`.`is_repost`, `a`.`reposted_post_id`,COUNT(`c`.`comment`) as'comment_count' FROM `users` as `b` LEFT JOIN `posts` as `a` ON `a`.`user_id`=`b`.`id` LEFT JOIN `comments` as `c` ON `a`.`id`=`c`.`post_id` WHERE `username`=? GROUP BY `a`.`id` ORDER BY `a`.`id` DESC LIMIT 20 OFFSET $offset";
        //GET like and repost count individually

        if ($preparedStmt=$conn->prepare($selectquery)) {
            $preparedStmt->bind_param("s",$username);
            $preparedStmt->execute();
            $result = $preparedStmt->get_result();
            $preparedStmt->close();
            return $result;
        }else "Could not get post. ".$conn->error;
    }

    function getAllPostsFor($conn,$userid,$page=0) {
        
        $offset = ($page > 1) ? ($page-1)*20 : 0;
    
        //Get following user ids, para WHERE user_id on ganun nalang
        $selectquery = "SELECT `a`.`id`, `b`.`full_name`,`b`.`username`,`b`.`profile_pic`, `a`.`content`, `a`.`picture`, `a`.`date_created`, `a`.`date_updated`, `a`.`is_repost`, `a`.`reposted_post_id`,COUNT(`c`.`comment`) as 'comment_count' FROM `posts` as `a` LEFT JOIN `users` as `b` ON `a`.`user_id`=`b`.`id` LEFT JOIN `comments` as `c` ON `c`.`post_id`=`a`.`id` WHERE `a`.`user_id` IN (SELECT `user_id` FROM `followers`  WHERE `follower_id`=?) OR `a`.`user_id`=? GROUP BY `a`.`id` ORDER BY `a`.`id` DESC LIMIT 20 OFFSET $offset";
        //GET like and repost count individually
        if ($stmt = $conn->prepare($selectquery)) {
            $stmt->bind_param("ii",$userid,$userid);
            $stmt->execute();
            $result = $stmt->get_result();

            $stmt->close();
            return $result;
        }else echo "Cant get posts at the moment";
    }

    function getAllPostsForCount($conn,$userid) {
        $getcount = "SELECT COUNT(`id`) as 'total_post_count' FROM `posts` WHERE `user_id` IN (SELECT `user_id` FROM `followers`  WHERE `follower_id`=?) OR `user_id`=?";
        if ($stmt = $conn->prepare($getcount)) {
            $stmt->bind_param("ii",$userid,$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['total_post_count'];
        }       
    }

    function getAllPostsByCount($conn,$username) {
        $getcount = "SELECT COUNT(`a`.`id`) as 'total_post_count' FROM `users` as `b` LEFT JOIN `posts` as `a` ON `a`.`user_id`=`b`.`id` WHERE `username`=?";

        if ($preparedStmt=$conn->prepare($getcount)) {
            $preparedStmt->bind_param("s",$username);
            $preparedStmt->execute();
            $result = $preparedStmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $preparedStmt->close();
            return $count['total_post_count'];
        }else "Could not get post. ".$conn->error;  
    }
    
    //Get single post, to see inside retweet, like that
    function getPost($conn,$postid) {
        $selectPost= "SELECT `a`.`id`, `b`.`full_name`,`b`.`username`,`b`.`profile_pic`, `a`.`content`, `a`.`picture`, `a`.`date_created`, `a`.`date_updated`, `a`.`is_repost`, `a`.`reposted_post_id`,COUNT(`c`.`comment`) as 'comment_count' FROM `posts` as `a` LEFT JOIN `users` as `b` ON `user_id`=`b`.`id` LEFT JOIN `comments` as `c` ON `c`.`post_id`=`a`.`id` WHERE `a`.`id`=? GROUP BY `a`.`id`;";
        //get like and repost count individually
        if ($preparedStmt=$conn->prepare($selectPost)) {
            $preparedStmt->bind_param("i",$postid);
            $preparedStmt->execute();
            $result = $preparedStmt->get_result();
            if ($result->num_rows == 0) {
                return 0;
            }
            $postInfo = $result->fetch_assoc();
            $result->free();
            $preparedStmt->close();
            return $postInfo;
        }
        return 0;
    }

    function insertPost($conn,$userid,$content,$picture,$isRepost=0,$repostedPostId=NULL) {
        $insertPost = "INSERT INTO `posts` (`user_id`, `content`,`picture`,`is_repost`, `reposted_post_id`) VALUES (?,?,?,?,?);";
    
        if ($stmt = $conn->prepare($insertPost)) {
            $stmt->bind_param("issii",$userid,$content,$picture,$isRepost,$repostedPostId);
            $result = $stmt->execute();
            $stmt->close();
            return $result;
        } echo "Could not Insert post as of now. ".$conn->error;
    }

    function deletePost($conn,$postid) {
        $directory = "../../public_html/images/attached/";
        $selectPicture = "SELECT `picture` FROM `posts` WHERE `id`=$postid";
        $result = $conn->query($selectPicture);
        $row = $result->fetch_assoc();
        $oldpicture = $row['picture'];
        $file = $directory.$oldpicture;         
        $delete = "DELETE FROM `posts` WHERE `id`=?";
        if ($stmt = $conn->prepare($delete)) {
            $stmt->bind_param("i",$postid);
            $result = $stmt->execute();
            $stmt->close();
            if ($result) {
                if (!is_null($oldpicture) && file_exists($file)) unlink($file);
                return $result;
            }else return "Could not delete. ".$conn->error;
        }else echo "Could not delete. ".$conn->error;
    }   

    function searchFor($conn,$rawkeyword,$category='people',$page=0) {
        $offset = ($page > 1) ? ($page-1)*20 : 0;

        $keyword = '%'.$rawkeyword.'%';
        //For people
        switch ($category) {
            case 'post':
            //SELECT COUNT(`id`) as 'total_search_count' FROM `posts` WHERE `content` LIKE '%a%' = 25
                $search = "SELECT `a`.`id`, `b`.`full_name`, `b`.`username`, `b`.`profile_pic`, `a`.`content`, `a`.`picture`, `a`.`date_created`, `a`.`date_updated`, `a`.`is_repost`, `a`.`reposted_post_id`,COUNT(`c`.`comment`) as 'comment_count' FROM `posts` as `a` LEFT JOIN `users` as `b` ON `user_id`=`b`.`id` LEFT JOIN `comments` as `c` ON `c`.`post_id`=`a`.`id` WHERE `content` LIKE ? GROUP BY `a`.`id` ORDER BY `a`.`id` DESC LIMIT 20 OFFSET $offset";
                break;
            default:
                //default ngaun ay people
            //SELECT COUNT(`id`) FROM `users` WHERE `username` LIKE '%a%' || `full_name` LIKE '%a%'; = 7
                $search = "SELECT `a`.`id`,`username`,`full_name`,`description`,`profile_pic`,COUNT(`user_id`) as 'post_count' FROM `users` as `a` LEFT JOIN `posts` ON `a`.`id`=`user_id` WHERE `username` LIKE ? || `full_name` LIKE ? GROUP BY `full_name` LIMIT 20 OFFSET $offset;";
                break;
        }

        if ($stmt = $conn->prepare($search)) {
            switch ($category) {
                case 'post':{
                    $stmt->bind_param("s",$keyword);
                    break;
                }
                default: {
                    // default as of now is pepople
                    $stmt->bind_param("ss",$keyword,$keyword);
                    break;
                }   
            }

            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }else echo "Could not search at the moment";
    }

    function searchForCount($conn,$rawkeyword,$category='people') {
        
        $keyword = '%'.$rawkeyword.'%';
        //For people
        switch ($category) {
            case 'post':
                $search = "SELECT COUNT(`id`) as 'total_search_count' FROM `posts` WHERE `content` LIKE ?";
                break;

            default: //default ngaun ay people
                $search = "SELECT COUNT(`id`) as 'total_search_count' FROM `users` WHERE `username` LIKE ? || `full_name` LIKE ?;";
                break;
        }
        if ($stmt = $conn->prepare($search)) {
            switch ($category) {
                case 'post':{
                    $stmt->bind_param("s",$keyword);
                    break;
                }
                default: {
                    // default as of now is pepople
                    $stmt->bind_param("ss",$keyword,$keyword);
                    break;
                }   
            }
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['total_search_count'];
        }else echo "Could not search at the moment";
    }

    function isAlreadyFollowing($conn,$user,$toFollow) {
        $select = "SELECT `id` FROM `followers` WHERE `user_id`=? AND `follower_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("ii",$toFollow,$user);
            $stmt->execute();
            $result = $stmt->get_result();
            $following = ($result->num_rows > 0 );
            $stmt->close();
            $result->free();
            return $following;
        }
    }

    function followUser($conn,$user,$toFollow) {
        $follow = "INSERT INTO `followers` (`user_id`, `follower_id`) VALUES (?,?);";
        if ($stmt = $conn->prepare($follow)) {
            $stmt->bind_param("ii",$toFollow,$user);
            $result = $stmt->execute();
            $stmt->close();
            return $result;
        }
        return false;
    }

    function unFollowUser($conn,$user,$toFollow) {
        $follow = "DELETE FROM `followers` WHERE `user_id`=? AND `follower_id`=?;";
        if ($stmt = $conn->prepare($follow)) {
            $stmt->bind_param("ii",$toFollow,$user);
            $result = $stmt->execute();
            $stmt->close();
            return $result;
        }
        return false;
    }

    function getProfileLikeCount($conn,$userid) {
        $select = "SELECT COUNT(`b`.`id`) as 'like_count' FROM `posts` as `a` LEFT JOIN `likes` as `b` ON `a`.`id`=`b`.`post_id` WHERE `a`.`user_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['like_count'];            
        }
    }

    function getProfileRepostCount($conn,$userid) {
        $select = "SELECT COUNT(`c`.`id`) as 'repost_count' FROM `posts` as `a` LEFT JOIN `posts` as `c` ON `a`.`id`=`c`.`reposted_post_id` WHERE `a`.`user_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['repost_count'];          
        }
    }

    function isAlreadyLiked($conn,$user,$post) {
        $select = "SELECT `id` FROM `likes` WHERE `user_id`=? AND `post_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("ii",$user,$post);
            $stmt->execute();
            $result = $stmt->get_result();
            $liked = ($result->num_rows > 0 );
            $stmt->close();
            $result->free();
            return $liked;
        }
    }

    function likePost($conn,$user,$postToLike) {
        $like = "INSERT INTO `likes` (`user_id`, `post_id`) VALUES (?,?);";
        if ($stmt = $conn->prepare($like)) {
            $stmt->bind_param("ii",$user,$postToLike);
            $result = $stmt->execute();
            $stmt->close();
            return $result;
        }
        return false;
    }

    function unlikePost($conn,$user,$postToUnlike) {
        $unlike = "DELETE FROM `likes` WHERE `user_id`=? AND `post_id`=?;";
        if ($stmt = $conn->prepare($unlike)) {
            $stmt->bind_param("ii",$user,$postToUnlike);
            $result = $stmt->execute();
            $stmt->close();
            return $result;
        }
        return false;
    }   

    function getUserPostCount($conn,$userid) {
        $select = "SELECT COUNT(`id`) as 'post_count' FROM `posts` WHERE `user_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['post_count'];            
        } 
    }

    function getPostLikeCount($conn,$postid) {
        $select = "SELECT COUNT(`id`) as 'like_count' FROM `likes` WHERE `post_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['like_count'];            
        } 
    }

    function getPostRepostCount($conn,$postid) {
        $select = "SELECT COUNT(`id`) as 'repost_count' FROM `posts` WHERE `reposted_post_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['repost_count'];          
        }
    }

    function getPostCommentCount($conn,$postid) {
        $select = "SELECT COUNT(`id`) as 'comment_count' FROM `comments` WHERE `post_id`=?";
        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['comment_count'];         
        }       
    }

    function getFollowingCount($conn,$userid) {
        $countFollowing = "SELECT COUNT(`id`) as 'following_count' FROM `followers` WHERE `follower_id`=?";
        if ($stmt = $conn->prepare($countFollowing)) {
            $stmt->bind_param("i",$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['following_count'];
        }else echo "Could not get count. ".$conn->error;
    }

    function getFollowerCount($conn,$userid) {
        $countFollowers = "SELECT COUNT(`id`) as 'follower_count' FROM `followers` WHERE `user_id`=?";
        if ($stmt = $conn->prepare($countFollowers)) {
            $stmt->bind_param("i",$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['follower_count'];
        }else echo "Could not get count. ".$conn->error;        
    }

    function viewFollowersOf($conn,$username,$page=0) {

        $offset = ($page > 1) ? ($page-1)*30 : 0;

        $selectFollowers = "SELECT `id`,`full_name`,`username`,`profile_pic` FROM `users` WHERE `id` IN (SELECT `follower_id` FROM `followers` WHERE `user_id`=(SELECT `id` FROM `users` WHERE `username`=?)) ORDER BY `full_name` LIMIT 30 OFFSET $offset;";

        if ($stmt = $conn->prepare($selectFollowers)) {
            $stmt->bind_param("s",$username);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }else echo "Could not get count. ".$conn->error;
    }

    function viewWhoYouAreFollowing($conn,$username,$page=0) {
        $offset = ($page > 1) ? ($page-1)*30 : 0;

        $selectFollowing = "SELECT `id`,`full_name`,`username`,`profile_pic` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `followers` WHERE `follower_id`=(SELECT `id` FROM `users` WHERE `username`=?)) ORDER BY `full_name` LIMIT 30 OFFSET $offset;";

        if ($stmt = $conn->prepare($selectFollowing)) {
            $stmt->bind_param("s",$username);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }else echo "Could not get count. ".$conn->error;
    }

    function viewWhoLiked($conn,$postid,$page=0) {
        $offset = ($page > 1) ? ($page-1)*30 : 0;

        $selectLiked = "SELECT `id`,`full_name`,`username`,`profile_pic` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `likes` WHERE `post_id`=?) ORDER BY `full_name` LIMIT 30 OFFSET $offset;";

        if ($stmt = $conn->prepare($selectLiked)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }else echo "Could not get count. ".$conn->error;
    }

    function viewWhoReposted($conn,$postid,$page=0) {
        $offset = ($page > 1) ? ($page-1)*30 : 0;

        $selectReposted = "SELECT `id`,`full_name`,`username`,`profile_pic` FROM `users` WHERE `id` IN (SELECT `user_id` FROM `posts` WHERE `reposted_post_id`=?) ORDER BY `full_name` LIMIT 30 OFFSET $offset;";

        if ($stmt = $conn->prepare($selectReposted)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }else echo "Could not get count. ".$conn->error;
    }

    function viewFollowersOfCount($conn,$username) {
        $getcount = "SELECT COUNT(`id`) as 'count' FROM `users` WHERE `id` IN (SELECT `follower_id` FROM `followers` WHERE `user_id`=(SELECT `id` FROM `users` WHERE `username`=?))";

        if ($stmt = $conn->prepare($getcount)) {
            $stmt->bind_param("s",$username);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['count'];
        }else echo "Could not get count. ".$conn->error;

    }
    function viewWhoYouAreFollowingCount($conn,$username) {
        $getcount = "SELECT COUNT(`id`) as 'count' FROM `users` WHERE `id` IN (SELECT `user_id` FROM `followers` WHERE `follower_id`=(SELECT `id` FROM `users` WHERE `username`=?))";

        if ($stmt = $conn->prepare($getcount)) {
            $stmt->bind_param("s",$username);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['count'];
        }else echo "Could not get count. ".$conn->error;        
    }

    function viewWhoLikedCount($conn,$postid) {

        $getcount = "SELECT COUNT(`id`) as 'count' FROM `users` WHERE `id` IN (SELECT `user_id` FROM `likes` WHERE `post_id`=?)";
        if ($stmt = $conn->prepare($getcount)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['count'];
        }else echo "Could not get count. ".$conn->error;    
    }
    function viewWhoRepostedCount($conn,$postid) {
        $getcount = "SELECT COUNT(`id`) as 'count' FROM `users` WHERE `id` IN (SELECT `user_id` FROM `posts` WHERE `reposted_post_id`=?)";
        if ($stmt = $conn->prepare($getcount)) {
            $stmt->bind_param("i",$postid);
            $stmt->execute();
            $result = $stmt->get_result();
            $count = $result->fetch_assoc();
            $result->free();
            $stmt->close();
            return $count['count'];
        }else echo "Could not get count. ".$conn->error;    
    }

    function editPost($conn,$postid,$content,$picture=NULL,$hasPicture) {

        $directory = "../../public_html/images/attached/";
        $selectPicture = "SELECT `picture` FROM `posts` WHERE `id`=$postid";
        $result = $conn->query($selectPicture);
        $row = $result->fetch_assoc();
        $oldpicture = $row['picture'];
        $file = $directory.$oldpicture;     

        if (is_null($picture) && $hasPicture) {
            //walang kasamang picture, pero originally may picture na = dont update picture
            $updatePost = "UPDATE `posts` SET `content`=? WHERE `id`=?";
        }else {
            //pag may kasamang picture change picture
            $updatePost = "UPDATE `posts` SET `content`=?,`picture`=? WHERE `id`=?";
        }

        if ($stmt = $conn->prepare($updatePost)) {

            if (is_null($picture) && $hasPicture) {
                $stmt->bind_param("si",$content,$postid);
            }else{
                $stmt->bind_param("ssi",$content,$picture,$postid);
            }
            $result = $stmt->execute();

            if ($result && !is_null($picture)) {
                //delete old picture
                if (!is_null($oldpicture) && file_exists($file)) unlink($file);
            }


            return $result;
        }else echo "Could not edit post. ".$conn->error;
    }

    function formattedDate($rawdate) {
        //galingan ang pag format ng time hehe
        //lagayan ng equivalents javascript
        $date = date_create($rawdate);
        return date_format($date,"Y/m/d g:i A");
    }

    function getAllPostIdsFor($conn,$userid,$page=0) {
        $offset = ($page > 1) ? ($page-1)*20 : 0;
        $getcount = "SELECT `id` FROM `posts` WHERE `user_id` IN (SELECT `user_id` FROM `followers`  WHERE `follower_id`=?) OR `user_id`=? ORDER BY `id` DESC LIMIT 20 OFFSET $offset";
        if ($stmt = $conn->prepare($getcount)) {
            $stmt->bind_param("ii",$userid,$userid);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }       
    }

    function getAllPostIdsBy($conn,$username,$page=0) {
        $offset = ($page > 1) ? ($page-1)*20 : 0;
        $getcount = "SELECT `a`.`id` FROM `users` as `b` LEFT JOIN `posts` as `a` ON `a`.`user_id`=`b`.`id` WHERE `username`=? ORDER BY `a`.`id` DESC LIMIT 20 OFFSET $offset";

        if ($preparedStmt=$conn->prepare($getcount)) {
            $preparedStmt->bind_param("s",$username);
            $preparedStmt->execute();
            $result = $preparedStmt->get_result();
            $preparedStmt->close();
            return $result;
        }else "Could not get post. ".$conn->error;  
    }

    function getSuggestionsFor($conn,$id) {
        //SELECT WHO YOU ARE NOT YET FOLLOWING
        $select = "SELECT `id`,`full_name`,`username`,`profile_pic` FROM `users` WHERE `id` NOT IN (SELECT `user_id` FROM `followers` WHERE `follower_id`=? ) AND `id`<>? ORDER BY RAND() DESC LIMIT 5";

        if ($stmt = $conn->prepare($select)) {
            $stmt->bind_param("ii",$id,$id);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            return $result;
        }
    }


?>