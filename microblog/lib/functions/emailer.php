<?php
    use PHPMailer\PHPMailer\PHPMailer;
    require_once ("../vendor/autoload.php");

    function sendVerification($to,$token) {
        $mail = new PHPMailer;
        $emailbody = <<<EOT
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Email Verification</title>
</head>
<body>
    <center>Thank you for registering on <strong>Tik-ti-laok</strong></center>
    <center>Activate your account by clicking the link below</center>
    <br>
    <br>
    <br>
    <center>http://localhost/exercises/microblog/res/checkemail.php?verify=$token</center>
</body>
</html>

EOT;
        $mail->From = "noreply@tiktilaok.com";
        $mail->FromName = "Tik-ti-laok";
        $mail->addAddress($to);
        $mail->Subject = "Email Verification";
        $mail->Body = $emailbody;
        $mail->AltBody =  $emailbody;
        $mail->IsHTML(true);
        if ($mail->send()) {
            return true;
        } else {
            echo "Mailer Error: " . $mail->ErrorInfo;
            return false;
        }            
    }
?>