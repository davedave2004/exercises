<?php
    session_start();
    include("../lib/functions/dbfunctions.php");
    if (isset($_SESSION['acctid'])) {
        $id = $_SESSION['acctid'];
        $acctInfo = getAcctInfo($mysqliconn,$id);   
    }
    $searchHome=0;
    if (isset($_GET['kw'])) {
        $keyword = $_GET['kw'];
        //people is the default search type;
        if (isset($_GET['find'])) {
            $categories = array('people','post');
            if (in_array($_GET['find'], $categories)) {
                $category = $_GET['find'];
            } else $category = 'people';
        } else $category = 'people';
        $totalsearchcount = searchForCount($mysqliconn,$keyword,$category);
        $availablePages = (int) (($totalsearchcount-1)/20)+1;
        $url = "search.php?kw=$keyword&find=$category&page=";
        if (isset($_GET['page']) && is_numeric($_GET['page'])) {
            $page = $_GET['page'];
            if ($page > $availablePages) {
                $page = 1;
            }
        } else {
            $page =1;
        }
        $backpage = $page-1;
        $forwardpage = $page+1;
        $searchResults = searchFor($mysqliconn,$keyword,$category,$page);
    } elseif(isset($_GET['home'])) {
        $searchHome =$_GET['home'];
    }
    else{
        header("Location: search.php?home=1");
        exit;
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?php echo (isset($keyword)) ? "Search: ".htmlentities($keyword): "Search"; ?></title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="js/scripts.js"></script>
</head>
<body>
  <?php if (isset($id)) : ?>
    <input type="hidden" id="acctid" value="<?php echo $id; ?>">
  <?php endif; ?>
  <!--Modal for posting and editing -->
  <div class="modal quick-post-modal">
    <div class="modal-content">
      <h2>Current Picture:</h2>
      <center>
        <img class="attached-picture" src="" alt="Current Picture">
      </center>
      <form action="" method="post" enctype="multipart/form-data" class="post-form" id="post-modal-form">
        <input type="hidden" name="user" value="<?php echo $id; ?>">
        <input type="hidden" name="do" value="post">
        <input type="hidden" name="hasPicture">
        <input type="hidden" name="post">
        <textarea name="content" form="post-modal-form" class="post-textarea" placeholder="Say something about this" maxlength="140"></textarea>
        <br>
        <span>
          <input type="file" name="attached">
          <button type="button" class="post-button quick-post-modal-submit">Post</button>
        </span>
      </form>
    </div>
  </div>
    <!--Modal for reposting -->
  <div class="higher-modal repost-modal">
    <div class="modal-content">
      <center>
        <h2 class="modal-header">Repost to followers</h2>
        <div class="modal-post">
          <a class="improvisedlink" href="">
            <img data-profile="" class="poster-pic viewprofile" src="" alt="Poster Pic">
          </a>
          <div class="post-text">
            <span>
              <a class="improvisedlink" href="">
                <strong data-profile="" class="viewprofile fullname"></strong>
                <i data-profile="" class="viewprofile username"></i>
              </a>
              <span class="date"></span>
            </span>
            <p class="content"></p>
          </div>
        </div>
        <div class="modal-postarea">
          <textarea class="post-textarea" placeholder="Say something about this" maxlength="140"></textarea>
          <button data-post class="post-button">Repost</button>   
        </div>
      </center>
    </div>
  </div>
    <!-- Viewing posts and comments -->
  <div class="modal post-modal">
    <div class="modal-content">
      <center>
        <div class="modal-post">
          <!-- dapat malagay ung data-profile -->
          <a class="improvisedlink" href="">
            <img data-profile="" class="poster-pic viewprofile" src="" alt="Poster Pic">
          </a>
          <div class="post-text">
            <span>
              <a class="improvisedlink" href="">
                <strong data-profile="" class="viewprofile fullname"></strong>
                <i data-profile="" class="viewprofile username"></i>
              </a>
              <span class="date"></span>
            </span>
            <p class="content"></p>
            <img class="attached-picture" src="" alt="Attached Image">
            <div class="reposted-post repost"></div>
          </div>
        </div>
        <div class="post-stats">
          <button title="Comment" class="show-comment-modal-button"></button><i class="view comment count"></i>
          <button title="Like" class="like-button"></button><i class="view like count"></i>
          <button title="Repost" class="repost-button"></button><i class="view repost-stat count"></i>
        </div>
        <div class="modal-postarea">
          <textarea name="commentarea" class="post-textarea" placeholder="Add a comment" maxlength="140"></textarea>
          <button data-post class="post-button  comment-button">Post</button> 
        </div>
        <div class="comment-section"></div>       
      </center>
    </div>
  </div> 
  <!-- Modal -->
  <!-- Header, if not signed it is still possible to view some pages-->
  <?php if (isset($acctInfo)) : ?>
    <div class="header">
      <a class="improvisedlink" href="home.php">
        <img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
        <span class="header-tiktilaok">Tik-ti-laok </span>
      </a>
      <div class="right-header">
        <form class="search-bar" method="get" action="search.php" onsubmit="return checkIfSearchIsEmpty();">
          <input type="text" name="kw" placeholder="Search">
        </form>
        <button class="post-button quick-post-button">Quick Post</button>
        <div class="profile-dropdown">
          <button class="profile-button"> Profile </button>
          <div class="dropdown-content">
            <a href="profile.php?user=<?php echo $acctInfo['username']?>">View Profile</a>
            <a href="edit.php">Edit Profile</a>
            <hr>
            <a href="logout.php" class="confirmation logout">Logout</a>
          </div>
        </div>
      </div>
    </div>
  <?php else: ?>
    <div class="header">
      <a class="improvisedlink" href="index.php">
        <img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
        <span class="header-tiktilaok">Tik-ti-laok </span>
      </a>
      <div class="right-header">
        <form class="search-bar" method="get" action="search.php" onsubmit="return checkIfSearchIsEmpty();">
          <input type="text" name="kw" placeholder="Search">
        </form>
      </div>
    </div>     
  <?php endif; ?>
  <!-- Header -->
  <div class="main-div">
    <?php if ($searchHome) : ?>
      <center>
        <h1>Search on Tik-ti-laok:</h1>
        <form class="search-bar-home" method="get" action="search.php" required>
          <input type="text" name="kw" placeholder="Search">
        </form>
      </center>
      <?php else : ?>
        <div class="search-result-keyword">
          <h2>Search Results for: 
            <i class="search-keyword"><?php echo htmlentities($keyword);?></i>
          </h2>
        </div>
        <div class="search-type-div">
            <form class="search-button-form" method="get" action="search.php">
              <input type="hidden" name="kw" value="<?php echo $keyword;?>">
              <input type="hidden" name="find" value="people">
              <input class="search-button <?php if (strcmp($category,'people')==0) echo 'active';?>" type="submit" value="People">
            </form>
            <form class="search-button-form" method="get" action="search.php">
              <input type="hidden" name="kw" value="<?php echo $keyword;?>">
              <input type="hidden" name="find" value="post">
              <input class="search-button <?php if (strcmp($category,'post')==0) echo 'active';?>" type="submit" value="Post">
            </form>
        </div>
        <!-- Search Result-->
        <div class="search-results">
          <?php switch($category):
           case 'post':  while ($post = $searchResults->fetch_assoc()) : ?>
            <!-- Post on Result-->
            <div class="modal-post original" data-id="<?php echo $post['id'] ?>" data-repost-id="<?php echo $post['reposted_post_id']; ?>">
              <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($post['username'])?>">
                <img class="poster-pic viewprofile" src="images/profile/<?php echo $post['profile_pic']?>" alt="Poster">
              </a>
              <div class="post-text">
                <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($post['username'])?>">
                  <strong class="viewprofile" data-profile="<?php echo htmlentities($post['username']);?>"><?php echo htmlentities($post['full_name']); ?></strong>
                  <i class="viewprofile username" data-profile="<?php echo htmlentities($post['username']);?>"><?php echo htmlentities("@".$post['username']);?></i>
                </a>
                <p class="content"><?php echo nl2br(htmlentities($post['content']));?></p>
                <?php if (!is_null($post['picture'])) : ?>
                  <img class="attached-picture" src="images/attached/<?php echo $post['picture'];?>" alt="Attached Image">
                <?php endif; ?>
                <?php if($post['is_repost']): $reposted = getPost($mysqliconn,$post['reposted_post_id']); ?>
                  <?php if($reposted): ?>
                    <div class="reposted-post repost">
                      <div class="post-text repost">
                        <a class="improvisedlink repost" href="profile.php?user=<?php echo htmlentities($post['username'])?>">
                          <strong class="viewprofile repost"><?php echo htmlentities($reposted['full_name']); ?></strong>
                          <i class="viewprofile username repost"><?php echo htmlentities("@".$reposted['username']);?></i>
                        </a>
                        <blockquote class="content repost"><?php echo nl2br(htmlentities($reposted['content']));?></blockquote>
                      </div>
                    </div>
                  <?php else: ?>
                    <div class="reposted-post repost greyed">
                      <div class="post-text repost">
                        <span class="">
                          <strong></strong>
                          <i></i>
                        </span>
                        <blockquote class="content repost">This post has been removed or deleted</blockquote>
                      </div>
                    </div>                              
                  <?php endif; ?>
                <?php endif; ?>
                <p>
                  <?php echo formattedDate($post['date_created']);?>
                </p>
                <div class="post-footer">
                  <div class="show-comments-button-div" data-post="<?php echo $post['id'];?>">
                    <button title="Comment" class="show-comments-button show-comment"></button>
                    <i class="view comment count show-comment"><?php echo $post['comment_count'] ?></i>
                  </div>                              
                  <div class="like-button-div" data-post="<?php echo $post['id'];?>">
                      <button title="Like" class="like-button <?php if (isAlreadyLiked($mysqliconn,$id,$post['id'])) echo 'liked';?>" ></button>
                      <i class="view like count"><?php echo getPostLikeCount($mysqliconn,$post['id']);?></i>
                  </div>
                  <div class="repost-button-div" data-post="<?php echo $post['id'];?>">
                      <button title="Repost" class="repost-button"></button>
                      <i class="view repost-stat count"><?php echo getPostRepostCount($mysqliconn,$post['id']);?></i>
                  </div>
                </div>                          
              </div>
              <!-- Post menu -->
              <?php if (isset($id) && strcmp($acctInfo['username'],$post['username'])==0) : ?>
                <div class="post-menu-dropdown">
                  <button class="post-menu-button">&dtrif;</button>
                  <ul class="post-menu-content" data-post="<?php echo $post['id'];?>">
                    <li class="edit-post-item">Edit</li>
                    <li class="delete-post-item">Delete</li>
                  </ul>
                </div>
              <?php endif; ?>
              <!-- End of post menu -->
            </div>                      
            <?php endwhile; break; 
            default: while ($searchResult = $searchResults->fetch_assoc()) : ?>
              <!-- User on result -->
              <div class="search-result-container" data-resid="<?php echo $searchResult['id'];?>" data-profile="<?php echo htmlentities($searchResult['username']); ?>">
                <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($searchResult['username'])?>">
                  <img class="search-pic viewprofile" src="images/profile/<?php echo $searchResult['profile_pic'];?>" alt="Profile Pic">
                </a>
                <div class="search-info-div">
                  <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($searchResult['username'])?>">
                    <h2 class="viewprofile"><?php echo htmlentities($searchResult['full_name']);?></h2>
                    <i class="viewprofile">@<?php echo htmlentities($searchResult['username']);?></i>
                  </a>
                  <p class="search-description-p"><?php echo nl2br(htmlentities($searchResult['description']));?></p>
                  <p class="stat-line">
                    <a class="view-link" href="view.php?view=followers&user=<?php echo $searchResult['username'];?>">
                      <strong class="strong-label view-follow">Followers: </strong>
                      <span class="follower-count"><?php echo getFollowerCount($mysqliconn,$searchResult['id']);?></span>
                    </a>
                  </p>
                  <p class="stat-line">
                    <a class="view-link" href="view.php?view=following&user=<?php echo $searchResult['username'];?>">
                      <strong class="strong-label view-follow">Following: </strong>
                      <span class="following-count"><?php echo getFollowingCount($mysqliconn,$searchResult['id']);?></span>
                    </a>
                  </p>
                  <span class="profile-stat-span">
                    <strong class="strong-label">Post Count: </strong>
                    <span class="post-count"><?php echo $searchResult['post_count'] ?></span>
                  </span>
                  <span class="profile-stat-span">
                    <strong class="strong-label">Like Count: </strong>
                    <?php echo getProfileLikeCount($mysqliconn,$searchResult['id']); ?></span>
                  <span class="profile-stat-span">
                    <strong class="strong-label">Repost Count: </strong>
                    <?php echo getProfileRepostCount($mysqliconn,$searchResult['id']); ?>
                  </span>
                </div>
                <!-- Follow Button -->
                <?php if (isset($id) && strcmp($acctInfo['username'],$searchResult['username'])!=0) : ?>
                    <button data-user="<?php echo $id; ?>" data-id="<?php echo $searchResult['id']?>" class="follow-button <?php if (isAlreadyFollowing($mysqliconn,$id,$searchResult['id'])) echo 'followed';?>"><?php if (isAlreadyFollowing($mysqliconn,$id,$searchResult['id'])){ echo 'Unfollow'; }else echo "Follow"; ?></button>
                <?php endif; ?>
              </div>
            <?php endwhile; break; 
          endswitch; $searchResults->free(); ?>
        </div>
        <!-- End of search result -->
        <?php endif; ?>
        <!-- Pagination: Display only when pages exceed 1 -->
        <?php if($searchHome==0 && ($availablePages>1)): ?>
        <div class="pagination-container">
          <div class="pagination">
            <?php if ($page>1) : ?>
              <a href="<?php echo '$url.$backpage'?>"> &laquo </a>
            <?php endif; ?>
            <?php for ($i=1; $i <= $availablePages; $i++) : ?>
              <a href="<?php echo $url.$i;?>" class="<?php if ($page==$i) echo "currentPage";?>"><?php echo $i?></a>
            <?php endfor; ?>
            <?php if ($page<$availablePages) : ?>
              <a href="<?php echo $url.$forwardpage?>"> &raquo </a>
            <?php endif; ?> 
          </div>
        </div>
        <?php endif; ?>
        <!-- End of Pagination -->   
      </div>
    </div>
    <script type="text/javascript">
        window.onload = function(){ 
            loadUINeeds();
            loadPostNeeds();
        }
    </script>
</body>
</html>