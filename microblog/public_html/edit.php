<?php
    session_start();
    include("../lib/functions/dbfunctions.php");
    include('../lib/functions/validation.php');

    if (!isset($_SESSION['acctid'])) {
        header("Location: index.php");
    } else {
        $errorMessage = "";
        $id = $_SESSION['acctid'];
        if (isset($_GET['edit'])) {
            $type = $_GET['edit'];
        } else {
            $type = "general";
        }
        if (strcmp($type,"general")==0) {
            if (isset($_POST['editprofile'])) {
                //get current acct info
                $proceedToUpdate = true;
                $fullname = $_POST['fullname'];
                $description = $_POST['description'];
                if (isset($_POST['username'])) {
                    //username was changed
                    $usernameError = "Changing usernames is not allowed for now.";
                    $proceedToUpdate = false;
                }
                if (isset($_POST['email'])) {
                    //email was changed
                    $emailError = "Changing email addresses is not allowed for now.";
                    $proceedToUpdate = false;
                }
                if (!checkNamePattern($fullname)) {
                    $nameError = "Please use a valid name";
                    $proceedToUpdate = false;
                }
                if(!checkPostLength($description)){
                    $descError = "Please limit it to 140 characters";
                    $proceedToUpdate = false;
                }
                if ($proceedToUpdate) {
                    if (updateProfInfo($mysqliconn,$id,$_POST)){
                        $confirmMessage = "Changes have been saved.";
                    } else {
                        $errorMessage = "Update failed";
                    }
                } else {
                    $errorMessage = "Update failed";
                }
            } elseif (isset($_FILES['newpicture'])) {
                $pictureFile = $_FILES['newpicture'];
                if ($pictureFile['error']==0) {
                    $fileType = pathinfo($pictureFile['name'],PATHINFO_EXTENSION);
                    echo "string";
                    $errorMessage .= validateFile($pictureFile,$fileType);
                    if (!$errorMessage) {
                        $directory = "images/profile/";
                        $filename = $directory.$id.date("ymdLHis").'.'.$fileType;
                        if (move_uploaded_file($pictureFile["tmp_name"], "$filename")) {
                            $completeFilename = explode("/", $filename);
                            $picture = $completeFilename[sizeof($completeFilename)-1];
                            if (!updateProfPic($mysqliconn,$id,$picture)) {
                                echo "Something went wrong with the upload";
                            }
                        } else $errorMessage .= "Upload Problem. ";
                    }
                }
            }
        } elseif (strcmp($type,"password")==0) {
            if (isset($_POST['newpass']) && isset($_POST['oldpass']) && isset($_POST['confnewpass'])) {
                $currentPass = $_POST['oldpass'];
                $newPass = $_POST['newpass'];
                $confNewPass = $_POST['confnewpass'];
                if (strcmp($newPass,$confNewPass)!=0) {
                    $newPassError = "Passwords do not match";
                }
                if (verifyPassword($mysqliconn,$id,$currentPass)) {
                //Password is correct
                    if (changePassword($mysqliconn,$id,$newPass)) {
                        $confirmMessage = "Password has been updated";
                    } else $errorMessage = "Password change failed";
                } else {
                    $oldPassError = "You have entered the wrong password";
                }
            }
        }
        $acctInfo = getAcctInfo($mysqliconn,$id);       
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <title>Edit Profile</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="js/jquery3.3.1.js"></script>
</head>
<body>
  <?php if (isset($id)) : ?>
    <input type="hidden" id="acctid" value="<?php echo $id; ?>">
  <?php endif; ?>
  <!--Modal for posting and editing -->
  <div class="modal quick-post-modal">
    <div class="modal-content">
      <h2>Current Picture:</h2>
      <center><img class="attached-picture" src="" alt="Current Picture"></center>
      <form action="" method="post" enctype="multipart/form-data" class="post-form" id="post-modal-form">
        <input type="hidden" name="user" value="<?php echo $id; ?>">
        <input type="hidden" name="do" value="post">
        <input type="hidden" name="hasPicture">
        <input type="hidden" name="post">
        <textarea name="content" form="post-modal-form" class="post-textarea" placeholder="Say something about this" maxlength="140"></textarea>
        <br>
        <span>
          <input type="file" name="attached">
          <button type="button" class="post-button quick-post-modal-submit">Post</button>
        </span>
      </form>
    </div>
  </div> 
  <!-- Header -->
  <div class="header">
    <a class="improvisedlink" href="home.php"><img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
    <span class="header-tiktilaok">Tik-ti-laok </span></a>
    <div class="right-header">
      <form class="search-bar" method="get" action="search.php" onsubmit="return checkIfSearchIsEmpty();">
        <input type="text" name="kw" placeholder="Search">
      </form>

      <button class="post-button quick-post-button">Quick Post</button>

      <div class="profile-dropdown">
        <button class="profile-button">PROFILE</button>
        <div class="dropdown-content">
          <a href="profile.php?user=<?php echo $acctInfo['username']?>">View Profile</a>
          <a href="edit.php">Edit Profile</a>
          <hr>
          <a href="logout.php" class="confirmation logout">Logout</a>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Header -->
  <div class="main-div">
    <div class="edit-type-div">
      <form class="edit-button-form" method="get" action="edit.php">
        <input type="hidden" name="edit" value="general">
        <input class="search-button <?php if (strcmp($type,'general')==0) echo 'active';?>" type="submit" value="General">
      </form>
      <form class="edit-button-form" method="get" action="edit.php">
        <input type="hidden" name="edit" value="password">
        <input class="search-button <?php if (strcmp($type,'password')==0) echo 'active';?>" type="submit" value="Password">
      </form>
    </div>
    <?php if (isset($errorMessage) && !empty($errorMessage)) : ?>
      <span class="error"><?php echo $errorMessage;?></span>
    <?php elseif (isset($confirmMessage)) : ?>
      <span class="success"><?php echo $confirmMessage;?></span>  
    <?php endif; ?>       
    <div class="edit-profile-div">
      <?php if (strcmp($type,"general")==0) : ?>
        <div>
          <img class="poster-pic" src="images/profile/<?php echo $acctInfo['profile_pic'];?>" alt="Profile Pic">
          <form name="pictureform" action="" method="post" enctype="multipart/form-data">
            <input class="upload-new-pic" type="file" name="newpicture">
          </form>
        </div>
        <form class="edit-form" id="editform" method="post" action="" onsubmit="return confirm('Save changes?')">
          <span class="error"><?php if (isset($usernameError)) echo $usernameError; ?></span>
          <p>
            <label class="edit-form-label" for="username">Username:</label>
            <input type="text" name="username" disabled value="<?php echo htmlentities($acctInfo['username']);?>">
          </p>
          <span class="error"><?php if(isset($emailError)) echo $emailError; ?></span>
          <p>
            <label class="edit-form-label" for="email">Email: </label>
            <input type="text" name="email" disabled value="<?php echo htmlentities($acctInfo['email'])?>">
          </p>
          <span class="error"><?php if(isset($nameError)) echo $nameError; ?></span>
          <p>
            <label class="edit-form-label" for="fullname">Name: </label>
            <input type="text" name="fullname" placeholder="Full Name" value="<?php echo $acctInfo['full_name']; ?>">
          </p>
          <span class="error"><?php if (isset($descError)) echo $descError; ?></span>
          <p>
            <label class="edit-form-label" for="description">Description: </label>
            <textarea class="description-textarea" form="editform" placeholder="Describle Yourself" name="description"><?php echo htmlentities($acctInfo['description']); ?></textarea>
          </p>
          <input type="submit" name="editprofile" value="Save">
        </form>
      <?php elseif (strcmp($type,"password")==0) : ?>
        <form name="editpassform" method="post" action="" onsubmit="return validateEditPassword();">
          <input type="hidden" name="acctid" value="<?php echo $id;?>">
          <span class="error oldpass-span"><?php if (isset($oldPassError)) echo $oldPassError; ?></span>
          <p>
            <label class="edit-form-label" for="oldpass">Current Password:</label>
            <input type="password" name="oldpass" required>
          </p>
          <hr>
          <span class="error newpass-span"><?php if(isset($newPassError)) echo $newPassError; ?></span>
          <p>
            <label class="edit-form-label" for="newpass">New Password:</label>
            <input type="password" name="newpass" required>
          </p>
          <p>
            <label class="edit-form-label" for="confnewpass">Confirm New Password:</label>
            <input type="password" name="confnewpass" required>
          </p>
          <center>
            <input type="submit" name="editpassword" value="Change Password">
          </center>
        </form>
      <?php endif ?>
    </div>
  </div> 
  <?php if ($errorMessage) echo "<script>alert(\"$errorMessage\");</script>"; ?>
  <script type="text/javascript">
    window.onload = function() {
      loadUINeeds();
      loadEditNeeds();
    }
  </script>
</body>
</html>