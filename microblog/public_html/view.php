<?php
    session_start();
    include("../lib/functions/dbfunctions.php");
    if (!isset($_GET['view'])) {
        header("Location: index.php");
        exit;
    } else {
        if (isset($_SESSION['acctid'])) {
        $id = $_SESSION['acctid'];
        $acctInfo = getAcctInfo($mysqliconn,$id);     
        }
        $view = $_GET['view'];
        if ($view == 'like' || $view == 'repost') {
          $url = "view.php?view=$view&post=$postid&page=";
        } else {
          $url = "view.php?view=$view&user=$user&page=";
        }
        if (isset($_GET['page']) && is_numeric($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page=1;
        }
        $backpage = $page-1;
        $forwardpage = $page+1;
        switch ($view) {
            case 'followers': {
                //Dapat nasa get kung kaninong account,
                if (isset($_GET['user'])) {
                    $user = $_GET['user'];
                    $count = viewFollowersOfCount($mysqliconn,$user);
                    $availablePages = (int) (($count-1)/30)+1;
                    if ($page > $availablePages) {
                        $page = 1;
                    }
                    $results = viewFollowersOf($mysqliconn,$user,$page);
                } else {
                    header("Location: index.php");
                    exit;
                }
                break;
            }
            case 'following': {
                //Dapat nasa get kung kaninong account, else own
                if (isset($_GET['user'])) {
                    $user = $_GET['user'];
                    $count = viewWhoYouAreFollowingCount($mysqliconn,$user);
                    $availablePages = (int) (($count-1)/30)+1;
                    if ($page > $availablePages) {
                        $page = 1;
                    }
                    $results = viewWhoYouAreFollowing($mysqliconn,$user,$page);
                } else {
                    header("Location: index.php");
                    exit;
                }
                break;                
            }
            case 'repost': {
                //Dapat nasa get kung aling post, else redirect
                if (isset($_GET['post'])) {
                    $postid = $_GET['post'];
                    $count = viewWhoRepostedCount($mysqliconn,$postid);
                    $availablePages = (int) (($count-1)/30)+1;
                    if ($page > $availablePages) {
                        $page = 1;
                    }
                    $results = viewWhoReposted($mysqliconn,$postid,$page);
                } else {
                    header("Location: index.php");
                    exit;
                }
                break;               
            }
            case 'like': {
                if(isset($_GET['post'])){
                    $postid = $_GET['post'];
                    $count = viewWhoLikedCount($mysqliconn,$postid);
                    $availablePages = (int) (($count-1)/30)+1;
                    if ($page > $availablePages) {
                        $page = 1;
                    }
                    $results = viewWhoLiked($mysqliconn,$postid,$page);
                } else {
                    header("Location: index.php");
                    exit;
                }
                break;    
            }      
            default: {
                header("Location: index.php");
                exit;
                break;
            }   
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?php echo "View ".htmlentities($view); ?></title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
  <?php if (isset($id)) : ?>
    <input type="hidden" id="acctid" value="<?php echo $id; ?>">
  <?php endif; ?>
  <!--Modal for posting and editing -->
  <div class="modal quick-post-modal">
    <div class="modal-content">
      <h2>Current Picture:</h2>
      <center>
        <img class="attached-picture" src="" alt="Current Picture">
      </center>
      <form action="" method="post" enctype="multipart/form-data" class="post-form" id="post-modal-form">
        <input type="hidden" name="user" value="<?php echo $id; ?>">
        <input type="hidden" name="do" value="post">
        <input type="hidden" name="hasPicture">
        <input type="hidden" name="post">
        <textarea name="content" form="post-modal-form" class="post-textarea" placeholder="Say something about this" maxlength="140"></textarea>
        <br>
        <span>
          <input type="file" name="attached">
          <button type="button" class="post-button quick-post-modal-submit">Post</button>
        </span>
      </form>
    </div>
  </div>
  <!-- Header, if not signed it is still possible to view some pages-->
  <?php if (isset($acctInfo)) : ?>
    <div class="header">
      <a class="improvisedlink" href="home.php">
        <img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
        <span class="header-tiktilaok">Tik-ti-laok </span>
      </a>
      <div class="right-header">
        <form class="search-bar" method="get" action="search.php" onsubmit="return checkIfSearchIsEmpty();">
          <input type="text" name="kw" placeholder="Search">
        </form>
        <button class="post-button quick-post-button">Quick Post</button>
        <div class="profile-dropdown">
          <button class="profile-button"> Profile </button>
          <div class="dropdown-content">
            <a href="profile.php?user=<?php echo $acctInfo['username']?>">View Profile</a>
            <a href="edit.php">Edit Profile</a>
            <hr>
            <a href="logout.php" class="confirmation logout">Logout</a>
          </div>
        </div>
      </div>
    </div>
  <?php else: ?>
    <div class="header">
      <a class="improvisedlink" href="index.php">
        <img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
        <span class="header-tiktilaok">Tik-ti-laok </span>
      </a>
      <div class="right-header">
        <form class="search-bar" method="get" action="search.php" onsubmit="return checkIfSearchIsEmpty();">
          <input type="text" name="kw" placeholder="Search">
        </form>
      </div>
    </div>     
  <?php endif; ?>
  <!-- Header -->
  <div class="main-div">
    <h1 class="view-category">
      <?php switch($view) {
        case 'followers': echo $user.'\'s followers'; break;
        case 'following': echo 'Following '.htmlentities($user); break;
        case 'repost': echo 'Reposted this post'; break;
        case 'like': echo 'Liked this post'; break;
        default: header("Location: index.php"); exit; break;}
      ?> 
      <span class="view-count-span"><?php echo $count;?></span>
    </h1>
    <div class="view-results">
      <?php while ($result = $results->fetch_assoc()) : ?>
        <div class="view-container">
          <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($result['username'])?>">
            <img class="view-picture viewprofile" src="images/profile/<?php echo $result['profile_pic']?>" alt="Profile Pic">
          </a>
          <div class="view-description">
            <center>
              <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($result['username'])?>">
                <strong class="viewprofile"><?php echo htmlentities($result['full_name']);?></strong>
                <br>
                <i class="viewprofile">@<?php echo htmlentities($result['username'])?></i>
              </a>
              <br>
              <?php if (isset($id) && strcmp($acctInfo['username'],$result['username'])!=0) : ?>
                <button data-user="<?php echo $id; ?>" data-id="<?php echo $result['id']?>" class="follow-button <?php if (isAlreadyFollowing($mysqliconn,$id,$result['id'])) echo 'followed';?>"><?php if (isAlreadyFollowing($mysqliconn,$id,$result['id'])) { echo 'Unfollow'; } else echo "Follow"; ?></button>
              <?php endif; ?>
            </center>
          </div>
        </div>
      <?php endwhile;?>
    </div>
    <?php if($availablePages>1): ?>
    <!-- Pagination: Display only when pages exceed 1 -->
    <div class="pagination-container">
      <div class="pagination">
        <?php if ($page>1) : ?>
          <a href="<?php echo $url.$backpage?>"> &laquo </a>
        <?php endif; ?>
        <?php for ($i=1; $i <= $availablePages; $i++) : ?>
          <a href="<?php echo $url.$i;?>" class="<?php if($page==$i) echo "currentPage";?>"><?php echo $i?></a>
        <?php endfor; ?>
        <?php if ($page<$availablePages) : ?>
           <a href="<?php echo $url.$forwardpage?>"> &raquo </a>
        <?php endif; ?>          
      </div>
    </div>
    <?php endif; ?>
    <!-- End of Pagination -->
  </div>
  <script type="text/javascript">
   window.onload = function() {
     loadUINeeds();
   }
  </script>
</body>
</html>