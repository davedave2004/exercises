<?php
    require_once('../lib/functions/emailer.php');
    include('../lib/functions/dbfunctions.php');
    $buttonvalue = "Resend Verification";
    $buttonaction = "checkemail.php?user=";
    if (isset($_GET['verify'])) {
        //check kung nasa database ung code
        $state = 'verify';
        $token = $mysqliconn->real_escape_string($_GET['verify']);
        $result = verifyEmail($mysqliconn,$token);
        if (is_array($result) && array_key_exists('id', $result)) {
            //valid ung token and nakuha ung id ng account;
            //check if may error
            $acctid = $result['id'];
            $fullname = $result['full_name'];
            $user = $result['username'];
            $buttonaction .= $user;

            if (array_key_exists('error', $result)) {
                $message = $result['error'];
            } else {
                $state = 'success';
            }
        } else {
            $message = $result;
        }
    } elseif (isset($_GET['user'])) {
        $state='user';
        $user = $_GET['user'];
        $buttonaction .= $user;
        if(isset($_GET['e'])) {
            //Hindi nasend ung email display iyon;
            $message = "Could not send verification at the moment. Try again later";
        } elseif (isset($_POST['resend'])) {
            //GET EMAIL AND TOKEN OF THE USER
            $credentials = getEmailAndToken($mysqliconn,$user); 
            $success = sendVerification($credentials['email'],$credentials['verification_token']);
            if ($success) {
                $message = "Sent! Check your email for the verification";
            } else $message = "Could not send verification at the moment. Try again later"; 
        }
    } else {
        header("Location: index.php"); 
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Email Verification</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <div class="header">
    <a class="improvisedlink" href="index.php"><img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
    <span class="header-tiktilaok">Tik-ti-laok </span></a>
  </div>
  <div class="main-div"> 
    <center>
    <?php if ($state==='user' && !isset($message)) : ?>
      Your account still needs to be verified before <br>
      You can start Tik-ti-laok-ing.
      <br>
      <br>
      Check your email or resend the email<br>
      by clicking on the button below
      <form class="verifyform" method="post" action="<?php echo $buttonaction;?>">
        <input class="verifybutton" type="submit" name="resend" value="<?php echo $buttonvalue;?>">
      </form> 
    <?php elseif ($state==='success') : ?>
      Your account has now been verified.
      <br>
      You can now start <strong>Tik-ti-laok</strong>-ing.
      <strong class="enjoy">Enjoy!</strong>
      <form class="verifyform" method="post" action="home.php">
        <input type="hidden" name="acctid" value="<?php echo $id; ?>">
        <input class="verifybutton" type="submit" name="resend" value="Let's Start Tik-ti-laok-ing">
      </form>
    <?php else : ?>
      <?php echo $message; ?>
    <?php endif; ?>
    </center>
  </div>
</body>
</html>