<?php
    session_start();
    include("../lib/functions/dbfunctions.php");

    if (isset($_SESSION['acctid'])) {
        header("Location: home.php");
    }
    if (isset($_POST['username']) && isset($_POST['password'])) {
        $acct = getLogin($mysqliconn,$_POST);
        if (is_array($acct)) {
            if ($acct['is_verified']) {
                $_SESSION['acctid'] = $acct['id'];
                header("Location: home.php");
            } else {
                $user = $acct['username'];
                header("Location: checkemail.php?user=$user");
            }
        } else $errorMessage = $acct;
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Tik-ti-laok</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
  <div class="main-div">
    <div class="login-container">
      <h1 class="tiktilaok"> Tik-ti-laok
        <img class="small-icon" src="images/logo/small-roostericon.png" alt="small icon">
      </h1>
      <form method="post" action="" class="login-form">
        <div class="login-div">
          <input class="input-text" type="text" name="username" placeholder="Username or Email">
          <input class="input-text" type="password" name="password" placeholder="Password">
        </div>
        <span class="login-span">
          <a href="register.php">Not yet registered?</a>
          <input class="login-button" type="submit" name="submit" value="Login">
        </span>
      </form>
    </div>
    <div class="info-div">
      <center class="center-icon">
        <img class="big-icon" src="images/logo/roostericon.png" alt="MALAKING MANOK">
        <p>Similar to Twitter</p>
        <p>Post like on twitter</p>
        <p>Follow like on twitter</p>
        <p>But unlike twitter ... its <strong>RED!</strong></p>
      </center>
    </div>
  </div>
  <?php if(isset($errorMessage)) echo "<script>alert('$errorMessage')</script>" ?>   
</body>
</html>