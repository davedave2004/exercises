<?php
    include("../lib/functions/dbfunctions.php");
    include('../lib/functions/validation.php');

    if (isset($_POST['acctid'])) {
        $id = $_POST['acctid'];
    } else exit("ERROR");
    $suggestions = getSuggestionsFor($mysqliconn,$id);
?>

<?php while ($suggest = $suggestions->fetch_assoc()) : ?>
  <div class="suggestion-container">
    <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($suggest['username']) ?>">
      <img class="suggestion-picture viewprofile" data-profile="<?php echo htmlentities($suggest['username'])?>" src="images/profile/<?php echo $suggest['profile_pic']?>" alt="Profile Pic">
    </a>
    <div class="view-description">                  
      <a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($suggest['username']) ?>">
      <strong class="viewprofile"><?php echo htmlentities($suggest['full_name']);?></strong>
      <br>
      <i class="viewprofile"><a class="improvisedlink" href="profile.php?user=<?php echo htmlentities($suggest['username']) ?>">@<?php echo htmlentities($suggest['username']);?></i></a>
    </div>
    <button data-user="<?php echo $id; ?>" data-id="<?php echo $suggest['id'] ?>" class="follow-button">Follow</button>
  </div>
<?php endwhile; $suggestions->free(); ?>

<script type="text/javascript">
  var followButtons = document.querySelectorAll(".follow-button");
  followButtons.forEach(button => {
    if (ifLoggedIn()) {
      button.addEventListener('click',ajaxFollow);
    }else button.addEventListener('click',showAlert);
  });
</script>