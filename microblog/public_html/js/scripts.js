function loadRegisterNeeds(){
    const errorpass = document.getElementById("error-pass");
    const passwordInput = document.getElementById("password");
    const confpassInput = document.getElementById("confpass");
    confpassInput.addEventListener("keydown",function(e) {
      clearTimeout(passTimeout);
      errorpass.textContent = "";
    });
    confpassInput.addEventListener("keyup",function(e) {
      passTimeout = setTimeout(checkPassword,3000);
    });   
}

function loadUINeeds() {
    const viewProfileElements = document.querySelectorAll(".viewprofile");
    //viewProfileElements.forEach(element => {element.addEventListener('click',viewProfileOnClick)});
    
    const followButtons = document.querySelectorAll(".follow-button");
    followButtons.forEach(button => {
        if (ifLoggedIn()) {
            button.addEventListener('click',ajaxFollow);
        } else button.addEventListener('click',showAlert);
    });

    const likeButtons = document.querySelectorAll(".like-button");
    likeButtons.forEach(button =>{
        if (ifLoggedIn()) {
            if (button.classList.contains("liked")) button.style.backgroundColor = "#d30313";
            button.addEventListener('click',ajaxLike);          
        } else button.addEventListener('click',showAlert);
    });

    const repostButtons = document.querySelectorAll(".repost-button");
    repostButtons.forEach(button => {
        if (ifLoggedIn()) {
            button.addEventListener('click',showRepostModal);
        } else button.addEventListener('click',showAlert);
    });

    const confirmationButtons = document.querySelectorAll(".confirmation");
    confirmationButtons.forEach(button => {
        button.addEventListener('click',function(e) {
            if (!confirm("Are you sure?")) e.preventDefault();
        });
    });

    const textareas = document.querySelectorAll("textarea");
    textareas.forEach(textarea => {
        textAreaGrow(textarea);
        textarea.addEventListener('input',function() {
            textAreaGrow(textarea);
        });
    });

    if (ifLoggedIn()) {
        const profileButton = document.querySelector(".profile-button");
        profileButton.addEventListener('click',showProfileDropdown);

        const postMenuButton = document.querySelectorAll(".post-menu-button");
        postMenuButton.forEach(button=>button.addEventListener('click',showPostMenu));

        const editButtons = document.querySelectorAll(".edit-post-item");
        editButtons.forEach(edits => edits.addEventListener('click',showEditPost));

        const deleteButtons = document.querySelectorAll(".delete-post-item");
        deleteButtons.forEach(deletes => deletes.addEventListener('click',ajaxDelete));

        const quickpost = document.querySelector(".quick-post-button");
        quickpost.addEventListener('click',showQuickPostModal);

        const quickPostSubmit = document.querySelector(".quick-post-modal-submit");
        quickPostSubmit.addEventListener('click',ajaxPost);
    }

    const searchResults = document.querySelectorAll(".search-result-container");
    searchResults.forEach(result => result.addEventListener('click',function(e) {
        if (!e.target.matches("button")) {
            window.location.href = `profile.php?user=${result.dataset.profile}`;
        }
    }));

    const viewlikes = document.querySelectorAll(".view.like");
    viewlikes.forEach(like => like.addEventListener('click',viewLikedBy));

    const viewreposts = document.querySelectorAll(".view.repost-stat");
    viewreposts.forEach(repost => repost.addEventListener('click',viewRepostedBy));

    const showCommentsButtons = document.querySelectorAll(".show-comment");
    showCommentsButtons.forEach(button => {
        button.addEventListener('click',function() {
            showPostModal(button.parentElement.dataset.post);
        });
    });

    //Dito show original post pero if may repost clickable ung repost para ma show ung repost
    const origPosts = document.querySelectorAll(".original");
    origPosts.forEach(post => {
        post.addEventListener('click',function(e) {
            if (!e.target.matches("button,strong,i,li")) {
                //if it is not a strong,i, or img tag, show post;
                if (e.target.matches(".repost")) {
                    const postToShow = post.dataset.repostId;
                    showPostModal(postToShow);
                } else {
                    const postToShow = post.dataset.id;
                    showPostModal(postToShow);
                }
            }
        });
        post.addEventListener("mouseover",function(e) {
            if (e.target.matches(".repost") ) {
                post.classList.remove("original");
            } else {
                if (!post.classList.contains("original")) {
                    post.classList.add("original");
                }
            }
        });             
    });

    window.onclick = function(e) {
        //dropdown onclicks
        if (!e.target.matches(".post-menu-button,.profile-button")) {
            closeDropdowns();
        }   
        if (e.target.matches(".repost-modal")) {
            document.querySelector(".repost-modal").classList.remove("show");
            document.body.classList.remove("modal-open");
        }
        if (e.target.matches(".post-modal")) {
            refreshProfileStats();
            document.querySelector(".post-modal").classList.remove("show");
            document.body.classList.remove("modal-open");
        }
        if (e.target.matches(".quick-post-modal")) {
            refreshProfileStats();
            document.querySelector(".quick-post-modal").classList.remove("show");
            document.body.classList.remove("modal-open");
        }
    }
}

    function loadEditNeeds() {
    const uploadpic = document.querySelector(".upload-new-pic");
    if (uploadpic) uploadpic.addEventListener("input",validateProfilePicture);
}

function textAreaGrow(textareaToGrow) {
    var textArea = textareaToGrow;
    const rowcount = textArea.value.split("\n").length;
    if (rowcount>3) {
        textArea.rows = rowcount;
    } else textArea.rows = 4;
}

function loadPostNeeds() {

    const postModal = document.querySelector(".post-modal");
    const postCommentArea = postModal.querySelector(".post-textarea");
    const postCommentButton = postModal.querySelector(".comment-button");
    postCommentArea.addEventListener('input',function(e) {
        if (!postCommentArea.value) {
            postCommentButton.disabled = true;
        } else postCommentButton.disabled = false;
    });

//TODO:
    const repostdiv = postModal.querySelector(".reposted-post");
    repostdiv.addEventListener('click',function(e) {
        ajaxGetFullPost(repostdiv.dataset.post);
    });

    const repostbutton = document.querySelector(".repost-modal .post-button");
    const commentButton = document.querySelector(".comment-button");
    if (ifLoggedIn()) {
        commentButton.addEventListener('click',ajaxComment);
        repostbutton.addEventListener('click',ajaxRepost);  
    } else {
        repostbutton.addEventListener('click',showAlert);   
        commentButton.addEventListener('click',showAlert);
    }

    const goToTextAreaButton = document.querySelector(".show-comment-modal-button");
    goToTextAreaButton.addEventListener('click',function(e) {
        document.querySelector(".post-modal").querySelector(".post-textarea").focus();
    });

}

function showPostModal(postid) {
    const postModal = document.querySelector(".post-modal");
    ajaxGetFullPost(postid);
}

function showRepostModal() {
    
    const repostModal = document.querySelector(".repost-modal");
    repostModal.querySelector(".post-textarea").value="";
    const button = this;
    const user = document.getElementById("acctid").value;

    const postid = button.parentElement.dataset.post;

    //data user ung nakalogin
    //data post ung id nung post
    
    ajaxGetPost(postid,user,repostModal);
}

function viewLikedBy() {
    const element = this;
    const post = element.parentElement.dataset.post;
    window.location.href = `view.php?view=like&post=${post}`;
}

function viewRepostedBy() {
    const element = this;
    const post = element.parentElement.dataset.post;
    window.location.href = `view.php?view=repost&post=${post}`;
}

function viewProfileOnClick() {
    //ILAGAY ANG data-profile SA MGA ELEMENTS NA KAILANGAN PMNTA SA PROFILE PAG CLINICK
    const element = this;
    var targetUser = element.dataset.profile;
    //const profile = element.dataset.profile;
    window.location.href = `profile.php?user=${targetUser}`; 
}

function ajaxPost() {
    const quickPostModal = document.querySelector(".quick-post-modal");
    const form = quickPostModal.querySelector("form");
    if (!validatePost(form)) {
        return false;

    }
    const formData = new FormData(form);
    var http = new XMLHttpRequest();
    var url = ('../lib/functions/forAJAX.php');
    http.open('POST',url,true);
    http.onreadystatechange = function() {
        if (http.readyState==4 && http.status==200) {

            try{
                refreshProfileStats();
                refreshFeed();
                //refreshing for search page
                if (ifLoggedIn()) {
                    const user = document.getElementById("acctid").value;
                    const results = document.querySelectorAll(".search-result-container");
                    const postCount = JSON.parse(http.responseText);
                    if (results) {
                        const yourResult = Array.from(results).filter(result => result.dataset.resid==user);
                        if (yourResult[0]) {
                            //update your count if you are also present in the search result
                            yourResult[0].querySelector(".post-count").textContent=postCount;
                        }
                    }                   
                } 

                alert("Posted!");
                quickPostModal.classList.remove("show");
                document.body.classList.remove("modal-open");
            }catch(e) {
                console.error(e);
                alert(http.responseText);
            }
        }
    }
    http.send(formData);

}

function ajaxCheckUsername(username) {
    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=checkuser&username=${username}`;
    http.open('POST',url,true);
    http.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if (http.readyState==4 && http.status == 200) {
            alert(http.responseText);
        }
    }
    http.send(params);
}

function ajaxGetFullPost(postid) {
    const postModal = document.querySelector(".post-modal");
    var user = "";
    if (ifLoggedIn()) user = document.getElementById("acctid").value;

    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=getfullpost&post=${postid}&user=${user}`;
    http.open('POST',url,true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if (http.readyState==4 && http.status ==200) {
            //gawing json
            try{
                const fullpostinfo = JSON.parse(http.responseText);
                const postinfo = fullpostinfo.main;
                const linkToProfile = postModal.querySelectorAll(".improvisedlink");
                const fullname = postModal.querySelector(".fullname");
                const username = postModal.querySelector(".username");
                const profpic =  postModal.querySelector(".poster-pic");
                const content = postModal.querySelector(".content");
                const date = postModal.querySelector(".date");
                const commentButton = postModal.querySelector(".comment-button");
                const commentCount = postModal.querySelector(".comment");
                const likeButton = postModal.querySelector(".like-button")
                const likecount = postModal.querySelector(".like");
                const repostcount = postModal.querySelector(".repost-stat");
                const attached = postModal.querySelector(".attached-picture");
                const repostdiv = postModal.querySelector(".reposted-post");
                const commentArea = postModal.querySelector(".post-textarea");
                const commentsection = document.querySelector(".comment-section");      

                //clean the modal fisr before displaying 
                //i.e remove the picture, the repost and the comments;
                attached.src = "";
                attached.style.display = "none";
                repostdiv.innerHTML="";
                commentsection.innerHTML="";
                commentArea.value="";
                commentArea.rows=4;
                commentButton.disabled = true;

                fullname.textContent = postinfo.full_name;
                
                username.textContent = "@"+postinfo.username;
                
                profpic.src = `images/profile/${postinfo.profile_pic}`;

                linkToProfile.forEach(link => link.href=`profile.php?user=${postinfo.username}`);

                content.innerText = postinfo.content;
                date.textContent = postinfo.date_created;
                commentCount.textContent = postinfo.comment_count;
                likecount.textContent = postinfo.like_count;
                likecount.parentElement.dataset.post = postinfo.id;
                repostcount.textContent = postinfo.repost_count;
                repostcount.parentElement.dataset.post = postinfo.id;

                commentButton.dataset.post = postinfo.id;
            
                if (postinfo.is_liked) { 
                    likeButton.classList.add("liked");
                    likeButton.style.backgroundColor = "#d30313";
                } else {
                    likeButton.classList.remove("liked");
                    likeButton.style.backgroundColor = "";
                }
                if (ifLoggedIn()) {
                    commentButton.dataset.user = document.getElementById('acctid').value;
                }

                //if post contains a picture, then display else hide
                if (postinfo.picture) {
                    attached.src = `images/attached/${postinfo.picture}`;
                    attached.style.display="inline-block";
                } else {
                    attached.src="";
                    attached.style.display="none";
                }
                //if the post is a repost, display the content of the reposted post 
                if (postinfo.is_repost) {
                    const repostinfo = fullpostinfo.repost;
                    repostdiv.dataset.post = postinfo.reposted_post_id;

                    var posttext = document.createElement("div");
                    posttext.classList.add("post-text","repost");

                    var link = document.createElement("a");
                    link.href=`profile.php?user=${repostinfo.username}`;
                    link.classList.add("repost","improvisedlink");

                    var strong = document.createElement("strong");
                    strong.classList.add("viewprofile","repost");

                    var italic = document.createElement("i");
                    italic.classList.add("username","repost","viewprofile");

                    var blockquote = document.createElement("blockquote");
                    blockquote.classList.add("content","repost");

                    if (repostinfo) {
                        strong.textContent = repostinfo.full_name;
                        italic.textContent = repostinfo.username;
                        blockquote.innerText = repostinfo.content;
                        
                        repostdiv.classList.remove("greyed");                               
                    } else {
                        blockquote.innerText = "This post has been removed or deleted";
                        repostdiv.classList.add("greyed");
                    }

                    link.appendChild(strong);
                    link.appendChild(italic);
                    posttext.appendChild(link);
                    posttext.appendChild(blockquote);
                    repostdiv.appendChild(posttext);
                    repostdiv.style.display = "block";
                } else repostdiv.style.display = "none";
                
                if (fullpostinfo.hasOwnProperty("comments")) {
                    comments = fullpostinfo.comments;
                    //loop thru comments
                    comments.forEach( comment => {
                        commentsection.appendChild(createCommentContainer(comment));
                    })
                    commentsection.style.display = "block";
                } else commentsection.style.display = "none";

                //after putting everything inside the modal, display the modal

                postModal.classList.add("show");
                document.body.classList.add("modal-open");
                commentArea.focus();

            }catch(e) {
                console.error(e);
                alert(http.responseText);
            }
        }
    }
    http.send(params);
}

function ajaxComment() {
    const button = this;
    const postid = button.dataset.post;
    const user = button.dataset.user;
    const postModal = document.querySelector(".post-modal");
    const content = postModal.querySelector(".post-textarea").value;

    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=comment&user=${user}&postid=${postid}&comment=${content}`;
    http.open('POST',url,true);
    http.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if (http.readyState==4 && http.status==200) {
            try{
                const commentCount = JSON.parse(http.responseText);
                const postInFeed = findPostFromFeed(postid);
                const countHolderInFeed = postInFeed.querySelector(".view.comment.count.show-comment");
                countHolderInFeed.textContent = commentCount;
                ajaxGetFullPost(postid);

            }catch(e) {
                console.error(e);
                alert("Comment could not be posted at the moment. ");
            }
        }
    }
    http.send(params);
}

function ajaxGetPost(postid,user,repostModal) {
    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=getpost&post=${postid}`;
    http.open('POST',url,true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if (http.readyState==4 && http.status ==200) {
            //gawing json
            try{
                const postinfo = JSON.parse(http.responseText);
                const linkToProfile = repostModal.querySelectorAll(".improvisedlink");
                const fullname = repostModal.querySelector(".fullname");
                const username = repostModal.querySelector(".username");
                const profpic =  repostModal.querySelector(".poster-pic");
                const repostbutton = repostModal.querySelector(".post-button");
                linkToProfile.forEach(link => link.href=`profile.php?user=${postinfo.username}`);
                fullname.textContent = postinfo.full_name;

                username.textContent = "@"+postinfo.username;

                profpic.src = `images/profile/${postinfo.profile_pic}`;

                repostModal.querySelector(".content").innerText = postinfo.content;
                repostModal.querySelector(".date").textContent = postinfo.date_created;
                repostbutton.dataset.post = postinfo.id;
                repostbutton.dataset.user = user;
                //after displaying everything show

                repostModal.classList.add("show");
                document.body.classList.add("modal-open");
            }catch(e) {
                console.error(e);
                alert(http.responseText);
            }

        }
    }
    http.send(params);
}

function ajaxRepost() {
    const button=this;

    const postModal = document.querySelector(".repost-modal");
    const textarea = postModal.querySelector(".post-textarea");
    const content = textarea.value;
    const postid = button.dataset.post;
    const user = document.getElementById("acctid").value;


    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=repost&post=${postid}&user=${user}&content=${content}`;
    http.open('POST',url,true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {
        if (http.readyState==4 && http.status ==200) {
            //gawing json
            try{
                const count = JSON.parse(http.responseText);
                
                if (document.querySelector(".post-modal.show")) {
                    //if the post modal is opened, update the count in the modal
                    const currentCountHolder = document.querySelector(".post-modal.show .view.repost-stat.count");
                    currentCountHolder.textContent = count;                 
                }
                const postContainer = findPostFromFeed(postid);
                if (postContainer.parentElement.matches(".search-results")) {
                    //this was reposted in the search page;
                    postContainer.querySelector(".repost-stat.count").textContent = count;
                }
                
                textarea.value = "";
                refreshFeed();
                refreshProfileStats();
                alert("Reposted!");
                postModal.classList.remove("show");
                document.body.classList.remove("modal-open");

            }catch(e) {
                console.error(e);
                alert(http.responseText);
            }
        }
    }
    http.send(params);
}

function ajaxFollow() {
    const button = this;
    const user = button.dataset.user;
    const toFollow = button.dataset.id;
    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=follow&user=${user}&follow=${toFollow}`;

    http.open('POST', url, true);
    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {//Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {

            try{
                const followerCount = JSON.parse(http.responseText);
                const followerCountHolder = button.parentElement.querySelector(".follower-count");
                if (followerCountHolder) followerCountHolder.textContent = followerCount.toFollow;

                //check If you are in the search results; only in search page
                const results = document.querySelectorAll(".search-result-container");
                if (results) {
                    const yourResult = Array.from(results).filter(result => result.dataset.resid==user);
                    if (yourResult[0]) {
                        //update your count if you are also present in the search result
                        yourResult[0].querySelector(".following-count").textContent=followerCount.user;
                    }
                }

                if (button.classList.contains("followed")) {
                    button.textContent = "Follow";
                    button.classList.remove("followed");
                } else {
                    button.textContent = "Unfollow";
                    button.classList.add("followed");
                }

                refreshProfileStats();
            }catch(e) {
                console.error(e);
                alert(http.responseText);
            }

        }
    }
    http.send(params);
}

function ajaxLike() {
    const button = this;
    const user = document.getElementById("acctid").value;
    const post = button.parentElement.dataset.post;
    var http = new XMLHttpRequest();
    var url = '../lib/functions/forAJAX.php';
    var params = `do=like&user=${user}&post=${post}`;

    http.open('POST', url, true);
    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = function() {//Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {
            
                //SUCCESFULL LIKE OR UNLIKE , 
                try{
                    const count = JSON.parse(http.responseText);
                    const currentCountHolder = button.nextElementSibling;

                    //also update the button in the feed;
                    const postInFeed = findPostFromFeed(post);
                    const countHolderInFeed = postInFeed.querySelector(".view.like.count");
                    const buttonInFeed = postInFeed.querySelector(".like-button");
                    
                    if (button.classList.contains("liked")) {
                        if (buttonInFeed !== button) {
                            buttonInFeed.style.backgroundColor = "";
                            buttonInFeed.classList.remove("liked");
                        }
                        button.style.backgroundColor = "";
                        button.classList.remove("liked");
                    } else {
                        if (buttonInFeed !== button) {
                            buttonInFeed.style.backgroundColor = "#d30313";
                            buttonInFeed.classList.add("liked");
                        }
                        button.style.backgroundColor = "#d30313";
                        button.classList.add("liked");
                    }
                    currentCountHolder.textContent = count;
                    if (countHolderInFeed !== currentCountHolder) {
                        countHolderInFeed.textContent = count;
                    }

                }catch(e) {
                    console.error(e);
                    alert(http.responseText);
                }

            refreshProfileStats();  
            
        }
    }
    http.send(params);
}

function showEditPost() {
    const postContainer = this.parentElement.parentElement.parentElement;
    
    var attachedpicture;
    if (postContainer.querySelector(".attached-picture")) {
        attachedpicture=postContainer.querySelector(".attached-picture").src;
    } else {
        attachedpicture=0;
    }
    const postId = postContainer.dataset.id;
    
    const postContent = postContainer.querySelector(".content").innerText;

    const postModal = document.querySelector(".quick-post-modal");
    //populate the modal for editing
    const editTextArea = postModal.querySelector(".post-textarea");
    const editHead = postModal.querySelector("h2");
    const editPicture = postModal.querySelector(".attached-picture");
    
    const editForm = postModal.querySelector("form");
    editForm.post.value=postId;
    editForm.attached.value="";
    

    if (postContainer.dataset.repostId) editForm.attached.style.visibility = "hidden";
    else {editForm.attached.style.display = "inline";}
    editTextArea.value=postContent;
    textAreaGrow(editTextArea);
    if (attachedpicture) {
        postModal.querySelector("form").hasPicture.value=1;
        editHead.innerHTML ="Current Picture:";
        editPicture.src=attachedpicture;
        editPicture.style.display = "block";
    } else {
        postModal.querySelector("form").hasPicture.value=0;
        editHead.innerHTML ="Current Picture: None";
        editPicture.src="";
        editPicture.style.display ="none";
    }
    postModal.classList.add("show");
    document.body.classList.add("modal-open");
}

function showQuickPostModal() {
    const postModal = document.querySelector(".quick-post-modal");
    const postPicture = postModal.querySelector(".attached-picture");
    const postTextArea = postModal.querySelector(".post-textarea");
    const postForm = postModal.querySelector("form");
    postForm.attached.style.visibility = "";
    postModal.querySelector("h2").innerHTML = "Post Something";
    postModal.querySelector("form").post.value="";
    postModal.querySelector("form").attached.value="";
    postTextArea.value="";

    textAreaGrow(postTextArea);
    postPicture.src="";
    postPicture.style.display ="none";
    postModal.classList.add("show");
    document.body.classList.add("modal-open");
}

function ajaxDelete() {
    const item = this;
    const postid = item.parentElement.dataset.post;
    const selectedPost = item.parentElement.parentElement.parentNode;
    if (confirm("Delete this post?")) {
        var http = new XMLHttpRequest();
        var url = '../lib/functions/forAJAX.php';
        var params = `do=delete&post=${postid}`;
        http.open('POST',url,true);
        http.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        http.onreadystatechange = function() {
            if (http.readyState==4 && http.status==200) {
                if (http.responseText == 1) {
                    alert('Post deleted!');
                    selectedPost.parentElement.removeChild(selectedPost);
                    refreshProfileStats();
                    refreshFeed();

                } else alert(http.responseText);
            }
        }
        http.send(params);

    }

}

function checkIfSearchIsEmpty() {
    const NotEmpty = !document.querySelector(".search-bar").elements["kw"].value;
    return !NotEmpty;
}

function validateEditPassword() {
  const form = document.forms.editpassform;
  const acctid = form.acctid.value;
  const pass = form.newpass.value;
  const confpass = form.confnewpass.value;
  const oldPassSpan = document.querySelector(".oldpass-span");
  const newPassSpan = document.querySelector(".newpass-span");
  if (pass && confpass) {
    //pass and conf pass are not empty
    if (pass === confpass) {
      newPassSpan.textContent="";
      return confirm("Continue changing password?");
    } else {
      //get span
      const newPassSpan = document.querySelector(".newpass-span");
      newPassSpan.textContent = "Passwords do not match";
    }
  } else {
    alert("Both are empty");
  }
  return false;
}

function showProfileDropdown() {
    const dropdown = document.querySelector(".dropdown-content");
    if (!dropdown.classList.contains("show")) {
        closeDropdowns();
    }
    dropdown.classList.toggle("show");
    if (!dropdown.classList.contains("show")) {
        this.blur();
    }
}

function showPostMenu() {
    const dropdown = this.parentElement.querySelector(".post-menu-content");
    if (!dropdown.classList.contains("show")) {
        closeDropdowns();
    }
    dropdown.classList.toggle("show");
    if (!dropdown.classList.contains("show")) {
        this.blur();
    }
}

function createCommentContainer(comment) {
    var container = document.createElement("div");
    container.classList.add("comment-container");

    var link = document.createElement("a");
    link.classList.add("improvisedlink");
    link.href = `profile.php?user=${comment.username}`;
    var linkForSpan = document.createElement("a");
    linkForSpan.href = `profile.php?user=${comment.username}`;
    linkForSpan.classList.add("improvisedlink");

    var commentpic = document.createElement("img");
    commentpic.src = `images/profile/${comment.profile_pic}`;
    commentpic.alt = "Commentor Pic";
    commentpic.classList.add("commentor-pic","viewprofile");

    var posttext = document.createElement("div");
    posttext.classList.add("post-text");

    var span = document.createElement("span");


    var strong = document.createElement("strong");
    strong.classList.add("viewprofile");
    strong.dataset.profile = comment.username;
    strong.textContent = comment.full_name;

    var italic = document.createElement("i");
    italic.classList.add("username","viewprofile");
    italic.dataset.profile = comment.username;
    italic.textContent = comment.username;

    var datespan = document.createElement("span");
    datespan.classList.add("date");
    datespan.textContent = comment.date_created;


    linkForSpan.appendChild(strong);
    linkForSpan.appendChild(italic);
    span.appendChild(linkForSpan);
    span.appendChild(datespan);

    var commentcontent = document.createElement("p");
    commentcontent.classList.add("content");
    commentcontent.innerText = comment.comment;

    posttext.appendChild(span);
    posttext.appendChild(commentcontent);

    link.appendChild(commentpic);
    container.appendChild(link);
    container.appendChild(posttext);

    return container;
}

function closeDropdowns() {
    //".quick-post-button,.quick-post,.post-menu-button,.profile-button"
    const dropdown = document.querySelector(".dropdown-content.show,.post-menu-content.show");
    if (dropdown) {
        dropdown.classList.remove("show");
    }
}

function ifLoggedIn() {
    if (document.getElementById('acctid')) {
        return true;
    } else {
        return false;
    }
}

function showAlert() {
    alert("You are not logged in!");
}

function validatePost(theform) {
    var errorMessages = [];

    if (theform.attached.files[0]) {
        const file = theform.attached.files[0];
        const result = checkFile(file);
        result.forEach(error => errorMessages.push(error));
        if (errorMessages.length!=0) theform.attached.value = "";
    } else {
        if (theform.content.value.length == 0) {
            //if no files are attached and post is empty
            return false;
        }
    }

    if (theform.content.value.length > 140) {
        errorMessages.push("Posts should not exceed 140 characters");
    }

    if (errorMessages.length != 0) {
        alert(errorMessages);
        return false;
    }
    return true;
}

function checkFile(file) {

    var errors = [];
    var validFileTypes = ["jpg","jpeg","png","gif"];

    var filetype = file.name.split(".").pop().toLowerCase();

    if (file.size > 5000000) {
        errors.push("File is too large");
    }

    if (!validFileTypes.includes(filetype)) {
        errors.push("File is not a valid picture");
    }
    return errors;
}

function refreshProfileStats() {
    var followerContainer = document.querySelector(".follower-count");
    var followingContainer = document.querySelector(".following-count");
    var likeContainer = document.querySelector(".like-count");
    var repostContainer = document.querySelector(".repost-count");
    var postContainer = document.querySelector(".post-count");
    var username = document.getElementById('username');

    if (likeContainer && repostContainer && username) {

        var http = new XMLHttpRequest();
        var url = '../lib/functions/forAJAX.php';
        var params = `do=getacctinfo&user=${username.value}&type=user`;
        http.open('POST',url,true);
        http.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        http.onreadystatechange = function() {
            if (http.readyState==4 && http.status==200) {
                try{
                    const profileInfo = JSON.parse(http.responseText);
                    followerContainer.textContent = profileInfo['follower_count'];  
                    followingContainer.textContent = profileInfo['following_count'];    
                    likeContainer.textContent = profileInfo['like_count'];  
                    postContainer.textContent = profileInfo['post_count'];  
                    repostContainer.textContent = profileInfo['repost_count'];  
                }catch(e) {
                    alert(http.responseText);
                }
            }
        }
        http.send(params);
    }
}

function validateProfilePicture() {
    path = this.value;
    pattern = /[\\/]/;
    basename = path.split(pattern).pop();

    var errorMessages = [];

    if (this.files[0]) {
    const file = this.files[0];
    const result = checkFile(file);
    result.forEach(error => errorMessages.push(error));
    }
    if (errorMessages.length != 0) {
    this.value="";
    alert(errorMessages);
    return false;
    }
    if (confirm(`Change profile pic to ${basename.split(".")[0]}?`)) { 
    document.forms.pictureform.submit();
    }
}

function createPostContainer(completePost) {
    //escape values
    const postInfo = completePost.main;
    const escapedUsername = htmlEntities(postInfo.username);

    const link = document.createElement("a");
    link.href=`profile.php?user=${escapedUsername}`;
    link.classList.add("improvisedlink");   

    const linkForSpan = document.createElement("a");
    linkForSpan.href=`profile.php?user=${escapedUsername}`;
    linkForSpan.classList.add("improvisedlink");

    const postDiv = document.createElement("div");
    postDiv.classList.add("modal-post","original");
    postDiv.dataset.id = postInfo.id;
    const postRepostId = postInfo.reposted_post_id;
    
    postDiv.dataset.repostId = (postRepostId != null) ? postInfo.reposted_post_id : "";

    const posterPic = document.createElement("img");
    posterPic.classList.add("poster-pic","viewprofile");
    posterPic.src = `images/profile/${postInfo.profile_pic}`;
    posterPic.alt = "Poster";

    const postText = document.createElement("div");
    postText.classList.add("post-text");

    const fullnameStrong = document.createElement("strong");
    fullnameStrong.classList.add("viewprofile");
    fullnameStrong.textContent = htmlEntities(postInfo.full_name); //escape

    const usernameI = document.createElement("i");
    usernameI.classList.add("viewprofile","username");
    usernameI.textContent = '@'+escapedUsername; //escape

    linkForSpan.appendChild(fullnameStrong);
    linkForSpan.appendChild(usernameI);

    const contentP = document.createElement("p");
    contentP.classList.add("content");
    contentP.innerText = postInfo.content; //escape

    const attachedPicture = document.createElement("img");
    if (postInfo.picture) {
        attachedPicture.classList.add("attached-picture");
        attachedPicture.src = `images/attached/${postInfo.picture}`;
        attachedPicture.alt = "Attached Image";
    }
    const repostDiv = document.createElement("div");
    if (postInfo.is_repost) {
        const repostInfo = completePost.repost;
        const escapedRepostUsername = htmlEntities(repostInfo.username);

        const linkForRepostSpan = document.createElement("a");
        linkForRepostSpan.href=`profile.php?user=${escapedRepostUsername}`;
        linkForRepostSpan.classList.add("improvisedlink","repost");

        if (repostInfo) {
            repostDiv.classList.add("reposted-post","repost");
        }
        else repostDiv.classList.add("reposted-post","repost","greyed");

        const repostText = document.createElement("div");
        repostText.classList.add("post-text","repost");

        const repostSpan = document.createElement("span");
        if (repostInfo) {
            repostSpan.classList.add("repost");
        }
    
        const repostFullnameStrong = document.createElement("strong");
        if (repostInfo) {
            repostFullnameStrong.classList.add("viewprofile","repost");
            repostFullnameStrong.dataset.profile = escapedRepostUsername; //escape
            repostFullnameStrong.textContent = htmlEntities(repostInfo.full_name); //escape
        }

        const repostUsernameI = document.createElement("i");
        if (repostInfo) {
            repostUsernameI.classList.add("viewprofile","username","repost");
            repostUsernameI.dataset.profile = escapedRepostUsername; //escape
            repostUsernameI.textContent = '@'+escapedRepostUsername; //escape           
        }

        const repostContent = document.createElement("blockquote");
        repostContent.classList.add("content","repost");
        if (repostInfo) {
            repostContent.innerText = repostInfo.content;
        }
        else repostContent.innerText = "This post has been removed or deleted";

        if (repostInfo) {
            linkForRepostSpan.appendChild(repostFullnameStrong);
            linkForRepostSpan.appendChild(repostUsernameI);
            repostSpan.appendChild(linkForRepostSpan);
        } else {
            repostSpan.appendChild(repostFullnameStrong);
            repostSpan.appendChild(repostUsernameI);            
        }

        repostText.appendChild(repostSpan);
        repostText.appendChild(repostContent);

        repostDiv.appendChild(repostText);
    }

    const dateP = document.createElement("p");
    dateP.innerText = postInfo.date_created; //format the date

    const postFooter = document.createElement("div");
    postFooter.classList.add("post-footer");

    const commentDiv = document.createElement("div");
    commentDiv.classList.add("show-comments-button-div");
    commentDiv.dataset.post=postInfo.id;

    const showCommentButton = document.createElement("button");
    showCommentButton.classList.add("show-comments-button","show-comment");
    showCommentButton.title = "Comment";
    showCommentButton.addEventListener('click',function(e) {
        showPostModal(showCommentButton.parentElement.dataset.post);
    });

    const commentCount = document.createElement("i");
    commentCount.classList.add("view","comment","count","show-comment");
    commentCount.textContent = postInfo.comment_count;
    commentCount.addEventListener('click',function(e) {
        showPostModal(showCommentButton.parentElement.dataset.post);
    });

    commentDiv.appendChild(showCommentButton);
    commentDiv.appendChild(commentCount);

    const likeDiv = document.createElement("div");
    likeDiv.classList.add("like-button-div");
    likeDiv.dataset.post=postInfo.id;

    const likeButton = document.createElement("button");
    likeButton.classList.add("like-button");
    
    if (postInfo.is_liked) {
        likeButton.style.backgroundColor = "#d30313";
        likeButton.classList.add("liked");
    }
    likeButton.title = "Like";
    likeButton.addEventListener('click',ajaxLike);

    const likeCount = document.createElement("i");
    likeCount.classList.add("view","like","count");
    likeCount.textContent = postInfo.like_count;
    likeCount.addEventListener('click',viewLikedBy)

    likeDiv.appendChild(likeButton);
    likeDiv.appendChild(likeCount); 

    const repostButtonDiv = document.createElement("div");
    repostButtonDiv.classList.add("repost-button-div");
    repostButtonDiv.dataset.post=postInfo.id;
    
    const repostButton = document.createElement("button");
    repostButton.classList.add("repost-button");
    repostButton.title = "Repost";
    repostButton.addEventListener('click',showRepostModal);

    const repostCount = document.createElement("i");
    repostCount.classList.add("view","repost-stat","count");
    repostCount.textContent = postInfo.repost_count;
    repostCount.addEventListener('click',viewRepostedBy)

    repostButtonDiv.appendChild(repostButton);
    repostButtonDiv.appendChild(repostCount);

    postFooter.appendChild(commentDiv)
    postFooter.appendChild(likeDiv)
    postFooter.appendChild(repostButtonDiv);

    postText.appendChild(linkForSpan);
    postText.appendChild(contentP);
    if (postInfo.picture) postText.appendChild(attachedPicture);
    if (postInfo.is_repost) postText.appendChild(repostDiv);
    postText.appendChild(dateP);
    postText.appendChild(postFooter);
    
    link.appendChild(posterPic);
    postDiv.appendChild(link);
    postDiv.appendChild(postText);
    
    const loggedInUser = document.getElementById("username");
    if (loggedInUser && loggedInUser.value === postInfo.username) {
        
        const menuDiv = document.createElement("div");
        menuDiv.classList.add("post-menu-dropdown");

        const menuButton = document.createElement("button");
        menuButton.classList.add("post-menu-button");
        menuButton.innerHTML = "&dtrif;";

        const menuContent = document.createElement("ul");
        menuContent.classList.add("post-menu-content");
        menuContent.dataset.post = postInfo.id;

        const editItem = document.createElement("li");
        editItem.classList.add("edit-post-item");
        editItem.appendChild(document.createTextNode("Edit"));

        const deleteItem = document.createElement("li");
        deleteItem.classList.add("delete-post-item");
        deleteItem.appendChild(document.createTextNode("Delete"));

        menuContent.appendChild(editItem);
        menuContent.appendChild(deleteItem);

        menuDiv.appendChild(menuButton);
        menuDiv.appendChild(menuContent);

        menuButton.addEventListener('click',showPostMenu);
        editItem.addEventListener('click',showEditPost);
        deleteItem.addEventListener('click',ajaxDelete);
        postDiv.appendChild(menuDiv);       
    }

    //addEventListeners
    const viewProfileElements = postDiv.querySelectorAll(".viewprofile");
    //viewProfileElements.forEach(element => {element.addEventListener('click',viewProfileOnClick)});

    postDiv.addEventListener('click',function(e) {
        if (!e.target.matches("button,strong,i,li")) {
            //if it is not a strong,i, or img tag, show post;
            if (e.target.matches(".repost")) {
                const postToShow = postDiv.dataset.repostId;
                showPostModal(postToShow);
            } else {
                const postToShow = postDiv.dataset.id;
                showPostModal(postToShow);
            }
        }
    });
    postDiv.addEventListener("mouseover",function(e) {
        if (e.target.matches(".repost") ) {
            postDiv.classList.remove("original");
        } else {
            if (!postDiv.classList.contains("original")) {
                postDiv.classList.add("original");
            }
        }
    });
    return postDiv;
}

//from CSS Tricks
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function findPostFromFeed(postid) {
    const divs = Array.from(document.querySelectorAll(".modal-post"));
    const matchedPost = divs.filter(div => div.dataset.id == postid);
    return matchedPost[0];
}

function refreshFeed() {
    const postFeed = document.querySelector(".post-feed");

    if (postFeed) {
        var page = 1;
        if (document.querySelector(".currentPage")) page = document.querySelector(".currentPage").textContent;
        var user = "";
        var params = "";
        var http = new XMLHttpRequest();
        var url = '../lib/functions/forAJAX.php';
        if (postFeed.classList.contains("home")) {
            user = document.getElementById("acctid").value;
            params = `do=refreshfeed&user=${user}&for=home&page=${page}`;
        } else {
            user = document.getElementById("username").value;
            if (ifLoggedIn()) {
                var acctid = document.getElementById("acctid").value;
                params = `do=refreshfeed&user=${user}&for=profile&page=${page}&acctid=${acctid}`;
            } else params = `do=refreshfeed&user=${user}&for=profile&page=${page}`;
        }
        http.open('POST',url,true);
        http.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        http.onreadystatechange = function() {
            if (http.readyState==4 && http.status==200) {
                try{
                    const newFeed = JSON.parse(http.responseText);
                    const currentPosts = Array.from(postFeed.children).reverse();;
                    
                    const firstChild = postFeed.firstChild;
                
                    while (postFeed.children.length) {
                        postFeed.removeChild(postFeed.firstChild);
                    }

                    newFeed.forEach(post => {
                        const newPost = createPostContainer(post);
                        
                        postFeed.appendChild(newPost);

                    });
                }catch(e) {
                    console.error(e);
                    alert(http.responseText);
                }
            }
        }
        http.send(params);      
    }
}

function checkPassword() {
  const regform = document.querySelector("form");
  const errorpass = document.getElementById("error-pass");
  if (regform.password.value !== regform.confpass.value) {
    errorpass.textContent = "Passwords does not match";
    return false;
  } else errorpass.textContent = "";
  return true;
}

function validateForm() {
  const regform = document.querySelector("form");
  const submitform = true;
  if (!checkPassword()) {
    return false;
  }
  if (submitform) return confirm("Proceed with registration?");
}
