<?php
    require_once('../lib/functions/emailer.php');
    include("../lib/functions/dbfunctions.php");
    include('../lib/functions/validation.php');
    //Check if post data is available and correct length
    if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['confpass']) && isset($_POST['fullname']) && isset($_POST['email'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $confpass = $_POST['confpass'];
        $fullname = $_POST['fullname'];
        $email = $_POST['email'];
        $proceedToRegister = true;
        $usernameError = "";
        $passwordError = "";
        $emailError = "";
        $nameError = "";   
        if (!checkUsernamePattern($username)) {
            $usernameError .= "Please use a valid username. ";
            $proceedToRegister = false;
        }
        if (!checkEmailPattern($email)) {
            $emailError .= "Please use a valid email. ";
            $proceedToRegister = false;     
        } 
        if (!checkNamePattern($fullname)) {
            $nameError .= "Your name is not valid. ";
            $proceedToRegister = false;  
        }
        $token = md5(rand(1,1000));
        //check if username is taken, query ka dito
        if (checkDataIfTaken($mysqliconn,$username,"username")) {
            $usernameError .= "Username already taken";
            $proceedToRegister = false;
        }
        //check if password matches
        if ($password !== $confpass) {
            $passwordError .= "Password does not match";
            $proceedToRegister = false;
        }
        if (checkDataIfTaken($mysqliconn,$email,"email")) {
            $emailError .= "Email is already in use <br>";
            $proceedToRegister = false;
        }
        if ($proceedToRegister) {
            $result = register($mysqliconn,$_POST,$token);
            if ($result) {
                $success = sendVerification($email,$token);
                if ($success) {
                    header("Location: checkemail.php?user=$username");
                } else header("Location: checkemail.php?e=1&user=$username");  
            }
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Tik-ti-laok: Register</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
  <div class="header">
    <a class="improvisedlink" href="index.php">
      <img class="header-icon" src="images/logo/small-roostericon.png" alt="small icon">
      <span class="header-tiktilaok">Tik-ti-laok</span>
    </a>
  </div>
  <center>
    <h1>Register Here!</h1>
  </center>
  <form method="post" action="" class="register-form" onsubmit="return validateForm();">
    <span id="error-user" class="error">
      <?php if (isset($usernameError)) echo $usernameError; ?>
    </span>
    <input id="username" class="register-input" type="text" name="username" placeholder="Username" value="<?php if (isset($username)) echo htmlentities($username); ?>">
    <span id="error-pass" class="error">
      <?php if (isset($passwordError)) echo $passwordError; ?>
    </span>
    <input id="password" class="register-input" type="password" name="password" placeholder="Password" >
    <input id="confpass" class="register-input" type="password" name="confpass" placeholder="Confirm Password" >
    <span id="error-email" class="error">
      <?php if (isset($emailError)) echo $emailError; ?>    
    </span>
    <input id="email" class="register-input" type="text" name="email" placeholder="Email Address" value="<?php if (isset($email)) echo htmlentities($email); ?>" >
    <span id="error-name" class="error">
      <?php if (isset($nameError)) echo $nameError; ?>
    </span>
    <input class="register-input" type="text" name="fullname" placeholder="Full name" value="<?php if (isset($fullname)) echo htmlentities($fullname); ?>">
    <input type="submit" name="submit" value="Register">
  </form>
  <script type="text/javascript">
    window.onload = function () {
      loadRegisterNeeds();
    }
  </script>
</body>
</html>