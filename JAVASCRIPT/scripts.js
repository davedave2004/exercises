function showAlert(message){
	alert(message);
}

function showConfirm(message){
	confirm(message);
}

function findPrimeNumbers(givenNum){
	var primeNumbers = [];
	for (var currentNum = 2; currentNum <= givenNum; currentNum++) {
		//starting from 2 lets check if the number is prime
		var numberIsPrime = true;
		//checking if the number is prime

		for(var i = 2; i<currentNum; i++){
			if(currentNum%i == 0){
				numberIsPrime = false;
				continue;
			}
		}
		if(numberIsPrime)primeNumbers.push(currentNum);
	}
	return primeNumbers;
}