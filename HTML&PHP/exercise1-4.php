<!DOCTYPE html>
<html>
<head>
	<title> Exercise 1-4 </title>
</head>
<body>
<form method="POST" action="">
	<div>Num 1: <input type="number" name="num1"></div>
	<div>
		<input type="submit" name="fizzbuzz" value="FizzBuzz!">
	</div>
</form>

<?php

if(isset($_POST["num1"])){
	$num1 = $_POST["num1"];

	for ($i=1; $i <= $num1 ; $i++) {
		if($i%3==0 && $i%5==0) echo "<strong>FizzBuzz</strong>";
		elseif ($i%5==0) echo "<strong>Buzz</strong>";
		elseif ($i%3==0) echo "<strong>Fizz</strong>";
		else echo  $i; 

		echo " ";
	}

}
?>


</body>
</html>