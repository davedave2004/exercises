<?php
$gcd = 0;

if(isset($_POST["num1"]) && isset($_POST["num2"])){
	$num1 = $_POST["num1"];
	$num2 = $_POST["num2"];
	
	$biggerNumber = max($num1,$num2);
	$smallerNumber = min($num1,$num2);

	while($biggerNumber%$smallerNumber != 0){
		$rem = $biggerNumber % $smallerNumber;
		$biggerNumber = $smallerNumber;
		$smallerNumber = $rem;	
	}
	$gcd = $smallerNumber;
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Exercise 1 -3</title>
</head>
<body>
<form method="POST" action="">
	<div>Num 1: <input type="number" name="num1"></div>
	<div>Num 2: <input type="number" name="num2"></div>
	<div>
		<input type="submit" name="gcd" value="Find GCD">
	</div>
</form>

<?php
	echo "<p> <strong> GCD: </strong> $gcd </p>";
?>


</body>
</html>