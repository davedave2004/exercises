<?php 
	$url = "exercise1-12.php?page=";
	$availablePages = 1;
?>

<!--
Get the page number, if none then 1
display only starting from 
page 1 = 0-9
page 2 = 10-19
page 3 = 20 - 29
-->
<!DOCTYPE html>
<html>
<head>
	<title>Exercise 1-12</title>
</head>
<body>
	
	<h2>Information Table</h2>

	<table>
		<tr><th>Username</th><th>Password</th><th>Email Address</th><th>Name</th><th>Age</th><th>Birthday</th><th>Picture</th></tr>

		<?php

		if(isset($_GET['page'])) $page = $_GET['page'];
		else $page = 1;

		$filename = "info.csv";
		if($filehandle = fopen($filename, "r")){
			$allinfo = fgetcsv($filehandle);
		};
		fclose($filehandle);

		if($allinfo){
			$infocount = sizeof($allinfo)-1;
			$availablePages = (int)($infocount/10)+1;
			$info = array_slice($allinfo, ($page-1)*10,10);

			foreach ($info as $key => $commaSepValue) {
				echo "<tr>";
				$data = explode(",", $commaSepValue);
				foreach ($data as $key => $value) {
					echo "<td>";
					if($key == 6) echo '<img src="'.$value.'" alt="Profile picture" width="100px" height="100px"'; //6 is the key of the profile picture
					else echo $value;
					echo "</td>";
				}
			echo "</tr>";
			}
		
		}	
		?>
		

		
	</table>
 
<!--
Put pagination here;

-->
	<div class="pagination">
		<?php
		$backpage = $page-1;
		$forwardpage = $page+1;
			if($page>1)	echo '<a href="'.$url.$backpage.'"> &laquo </a>';

			for ($i=1; $i <= $availablePages; $i++) {
				//if(page is current page set stype to active) 
				echo '<a href="'.$url.$i.'"> '.$i.' </a>';
			}

			if($page<$availablePages) echo '<a href="'.$url.$forwardpage.'"> &raquo </a>';	  	
		?>
	</div> 


</body>
</html>