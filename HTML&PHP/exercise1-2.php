<?php
$result = 0;
if(isset($_POST["num1"]) && isset($_POST["num2"])){
	$num1 = $_POST["num1"];
	$num2 = $_POST["num2"];

	if(isset($_POST["add"])) $result = $num1 + $num2;
	if(isset($_POST["sub"])) $result = $num1 - $num2;
	if(isset($_POST["div"])) $result = $num1 / $num2;
	if(isset($_POST["mul"])) $result = $num1 * $num2; 
}


?>

<!DOCTYPE html>
<html>
<head>
	<title> Exercise 1-2 </title>
</head>
<body>
<form method="POST" action="">
	<div>Num 1: <input type="number" name="num1"></div>
	<div>Num 2: <input type="number" name="num2"></div>
	<div>
		<input type="submit" name="add" value="Add">
		<input type="submit" name="sub" value="Subtract">
		<input type="submit" name="div" value="Divide">
		<input type="submit" name="mul" value="Multiply">
	</div>
</form>

<?php 
	echo "<p> <strong> Result: </strong> $result </p>";
?>


</body>
</html>