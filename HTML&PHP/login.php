<?php
	session_start();
	if(isset($_SESSION['key'])) header("Location: profilepage.php");
	elseif(isset($_POST['username']) && isset($_POST['password'])){
		//check credentials
		$message ="";
		$username = $_POST['username'];
		$password = $_POST['password'];

		//1. read csv get all info

		$filename = "info.csv";
		if($filehandle = fopen($filename, "r")){
			$allinfo = fgetcsv($filehandle);
		}
		fclose($filehandle);

		if($allinfo){
			$gotAMatch = false;
			foreach ($allinfo as $key => $infoString) {
				$infoArray = explode(",", $infoString);
				if($infoArray[0] === $username) {
					$gotAMatch = true;
					$message = "Hi ".$infoArray[3];
					if($infoArray[1]===$password){
						$_SESSION['key'] = $key;
						unset($message);
						header("Location: profilepage.php");
					}else  $message .= ", your password is wrong";
				}
			}
		}
		if(!$gotAMatch){
			$message = "Not an exising user";
		}

		if(isset($message)){
		$_SESSION['errormessage'] = $message;	
		header("Location:exercise1-13.php");
		}

	}else header("Location:exercise1-13.php");
?>