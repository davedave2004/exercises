<?php

if(isset($_POST)){

	$info = array();
	foreach ($_POST as $key => $value) {
		if(strcmp($value, "Submit")==0) break;
		echo "<p><strong>".ucfirst($key).":</strong> ".$value."</p>";
		array_push($info, $value);

	}
	//Uploading Picture
	$directory = "images/";
	$validFileTypes = array("jpg","jpeg","png","gif");
	$filename = $directory .date("ymdLHis").basename($_FILES["picture"]["name"]);

	$imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
	if(!in_array($imageFileType, $validFileTypes)){
		echo "Not a valid picture";
	}
	elseif($_FILES["picture"]["size"] > 500000){
		echo "Image is too big";
	}
	else{
		move_uploaded_file($_FILES["picture"]["tmp_name"], "$filename");
		
		echo '<img src="'.$filename.'"alt="Profile picture" width="200" height="200">';

		//Writing to CSV
		$infostring = '"'.implode(",", $info).','.$filename.'"';
		 
		$filename = "info.csv";
		$filehandle = fopen($filename, "a");
		if($filehandle){
			if(filesize($filename)!=0) fwrite($filehandle, ",");
			fwrite($filehandle, $infostring);
			echo "Stored to info.csv";
		}
		fclose($filehandle); 

		echo "Picture uploaded succesfully";
	}
}

?>