<!DOCTYPE html>
<html>
<head>
	<title> Exercise 1-7 </title>
</head>
<body>
<form method="POST" action="displayinfo.php">
	<div>
		<div>
			<label>Username: </label><input type="text" name="username" placeholder="Username" required>
		</div>
		<div>
			<label>Password: </label><input type="password" name="password" placeholder="Password" pattern=".{6,20}" required>
		</div>
		<div>
			<label>Email: </label><input type="text" name="email" placeholder="Email" pattern="[A-Za-z0-9.]*@[A-Za-z]*[.].*"required>
		</div>
		<div>
			<label>Name: </label><input type="text" name="name" placeholder="Name" pattern="[A-Za-z ]*" required>
		</div>
		<div>
			<label>Age: </label><input type="number" name="age" placeholder="Age" min="0" max="150" required>
		</div>
		<div>
			<label>Birthday: </label><input type="date" name="bday" placeholder="Birthday" required>
		</div>
		<input type="submit" name="submit" value="Submit">
	</div>
</form>

<?php

?>


</body>
</html>