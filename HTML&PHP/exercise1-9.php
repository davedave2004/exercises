<!DOCTYPE html>
<html>
<head>
	<title>Exercise 1-9</title>
</head>
<body>
	<h2>Information Table</h2>

	<table>
		<tr><th>Username</th><th>Password</th><th>Email Address</th><th>Name</th><th>Age</th><th>Birthday</th></tr>
		<?php

		$filename = "info.csv";
		if($filehandle = fopen($filename, "r")){
			$info = fgetcsv($filehandle);
		};
		fclose($filehandle);

		foreach ($info as $key => $commaSepValue) {
			echo "<tr>";
			$data = explode(",", $commaSepValue);
			foreach ($data as $key => $value) {
				echo "<td>".$value."</td>";
			}
			echo "</tr>";
		}
		?>
	</table>

</body>
</html>