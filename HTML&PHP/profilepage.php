<?php
	session_start();
	if(!isset($_SESSION['key'])) header("Location: exercise1-13.php");
	else $key = $_SESSION['key'];

		$filename = "info.csv";
		if($filehandle = fopen($filename, "r")){
			$allinfo = fgetcsv($filehandle);
		}
		fclose($filehandle);

		$infoString = $allinfo[$key];
	
		$info = explode(",", $infoString);
		$email = $info[2];
		$name = $info[3];
		$age = $info[4];
		$birthday = $info[5];
		$profilepic = $info[6];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
</head>
<body>
	<div>
		<?php echo '<img src="'.$profilepic.'" alt="Profile Pic" width="100px" height="100px">';?>
	</div>
	<div>
		<p>Name: <?php echo $name ?></p>
		<p>Email: <?php echo $email ?></p>
		<p>Age: <?php echo $age ?></p>
		<p>Birthday: <?php echo $birthday ?></p>
	</div>
	



	<form action="logout.php"><input type="submit" value="Log Out"></form>
</body>
</html>