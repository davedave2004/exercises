<?php
session_start();
	if(isset($_SESSION['key'])) header("Location: profilepage.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
</head>
<body>
	
	<div>
		<form method="post" action="login.php">
			<label>Username: </label><input type="text" name="username" required>
			<label>Password: </label><input type="password" name="password" pattern=".{6,}" required>
			<input type="submit" value="Login">
		</form>

		<a href="exercise1-10.php">Register</a>
	</div>


	<?php
		if(isset($_SESSION['errormessage'])) {
			echo $_SESSION['errormessage'];
			unset($_SESSION['errormessage']);
		}
	?>

</body>
</html>