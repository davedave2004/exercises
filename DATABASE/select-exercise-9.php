<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 9</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 9) Retrieve department name which has no employee.</h2>

	<?php 
		$query = "SELECT `Name` FROM `departments` WHERE `id` NOT IN (SELECT `department_id` FROM `employees` GROUP BY `department_id`);";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>