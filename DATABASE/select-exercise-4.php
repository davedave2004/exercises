<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 4</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</h2>

	<?php 
		$query = "SELECT `emp`.`last_name` AS 'Employee\'s Last Name', `boss`.`last_name` AS 'Boss\' Last Name' FROM `employees` AS `emp` INNER JOIN (SELECT `id`,`last_name` FROM `employees` WHERE `id` IN (SELECT DISTINCT `boss_id` FROM `employees`)) AS `boss` ON `emp`.`boss_id` = `boss`.`id` WHERE `emp`.`boss_id` IS NOT NULL;";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>