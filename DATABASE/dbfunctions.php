<?php 
	require('db.php');

	function queryThenDisplay($conn,$query){
		if($preparedStmt = $conn->prepare($query)){
			$preparedStmt->execute();
			$result = $preparedStmt->get_result();
			$preparedStmt->close();
			$firstrow = $result->fetch_array(MYSQLI_ASSOC);
			
			echo "<table><tr>";
			foreach ($firstrow as $key => $value) {
				echo "<th>";
				echo $key;
				echo "</th>";
			}
			echo "</tr>";
			$result->data_seek(0);
			while($row=$result->fetch_assoc()){
				echo "<tr>";
				foreach ($row as $key => $value) {
						echo "<td>";
						echo $value;
						echo "</td>";
				}
				echo "</tr>";
			}
			echo "</table>";			
		}
		return;
	}


	function getAccountInfo($conn,$acctid){
		$selectquery = "SELECT username,email,first_name,last_name,bday,contactno,file_name FROM users LEFT JOIN profilepictures ON users.id = acct_id WHERE users.id=?";

		if($preparedStmt = $conn->prepare($selectquery)){
			$preparedStmt->bind_param('i',$acctid);
			$preparedStmt->execute();
			$result = $preparedStmt->get_result();
			$preparedStmt->close();
			return $result->fetch_array(MYSQLI_ASSOC);
		}
	}

	function getAllAccountInfo($conn){
		$selectAll = "SELECT users.*,profilepictures.file_name FROM users LEFT JOIN profilepictures ON users.id = acct_id";

		if($stmt = $conn->prepare($selectAll)){
			$stmt->execute();
			$result = $stmt->get_result();
			$stmt->close();
			return $result;
		}else echo "Something went wrong ".$conn->error;

	}

	function insertInfo($conn,$userInfo){
		$insertquery = "INSERT INTO users (username,password,email,first_name,last_name,bday,contactno) VALUES (?,?,?,?,?,?,?)";

		if($prepInsert = $conn->prepare($insertquery)){
			$prepInsert->bind_param("sssssss",$userInfo['username'],$userInfo['password'],$userInfo['email'],$userInfo['firstname'],$userInfo['lastname'],$userInfo['bday'],$userInfo['contactno']);
			if($prepInsert->execute()){
				echo "You are now registered!";
			} else echo $conn->error;
		} else echo $prepInsert->error;
		$prepInsert->close();
	}

	function insertProfPic($conn,$acctid,$filename){
		//Check if he already has a profilepic, if yes then update only, else insert
		$select = "SELECT id,file_name FROM profilepictures WHERE acct_id=?";
		if($checkProfPicStmt = $conn->prepare($select)){
			$checkProfPicStmt->bind_param('i',$acctid);
			$checkProfPicStmt->execute();
			$result = $checkProfPicStmt->get_result();
			$checkProfPicStmt->close();
			if($result->num_rows>0){
				//Already has a picture
				$id = $result->fetch_row();
				$update = "UPDATE profilepictures SET file_name=? WHERE id=?";
				if($updatePicStmt = $conn->prepare($update)){
					$updatePicStmt->bind_param('si',$filename,$id[0]);
					$result = $updatePicStmt->execute();
					$updatePicStmt->close();
					
					//delete previous file
					unlink("profpics/".$id[1]);
					return $result;
				}
			}else{
				//Doesnt have a picture yet
				$insert = "INSERT INTO profilepictures (acct_id,file_name) VALUES (?,?);";
				if($prepared = $conn->prepare($insert)){
					$prepared->bind_param("is",$acctid,$filename);
					$result = $prepared->execute();
					$prepared->close();
					return $result;
				}
			}
		}
	}

	function checkDataIfTaken($conn,$data,$category){
		$selectquery = "SELECT $category FROM users WHERE $category=?";

		if($prepSelect = $conn->prepare($selectquery)){
			$prepSelect->bind_param('s',$data);
			$prepSelect->execute();
			$result = $prepSelect->get_result();
			$prepSelect->close();
			return ($result->num_rows >0);
		} else echo $conn->error;
	}

	function getLogin($conn,$data){
		//Check first if username is registered
		if(checkDataIfTaken($conn,$data['username'],"username")){
			//Check if username and password matches the data in db
			$selectquery = "SELECT id FROM users WHERE username=? AND password =?";
			if($prepSelect = $conn->prepare("$selectquery")){
				$prepSelect->bind_param("ss",$data['username'],$data['password']);
				$prepSelect->execute();
				$result = $prepSelect->get_result();
				$prepSelect->close();
				if($result->num_rows>0){
					$row = $result->fetch_array(MYSQLI_ASSOC);
					return	$row['id'];
				}else echo "Wrong account or password";
			}
		} else echo "That user is not registered";
		return false;
	}
?>