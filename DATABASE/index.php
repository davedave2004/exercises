<?php
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		header("Location: profile.php");
	}

	if(isset($_POST['username']) && isset($_POST['password'])){
		$acctid = getLogin($mysqliconn,$_POST);
		if($acctid){
			$_SESSION['acctid'] = $acctid;
			header("Location: profile.php");
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>DB Exercise: Login</title>
</head>
<body>
	<div class="loginform">
		<form method="post" action="">
			<label>Username: </label><input class="logintextbox" type="text" id="username" name="username">
			<label>Password: </label><input class="logintextbox" type="password" id="password" name="password">
			<input type="submit" name="submit" value="Enter">
		</form>
		<a href="register.php">Register Here</a>
		<a href="display.php">View All Data Here</a>
	</div>
</body>
</html>