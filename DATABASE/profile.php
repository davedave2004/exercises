<?php 
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		$acctid = $_SESSION['acctid'];
	}else header("Location: index.php");
	//FETCH LAHAT NG VALUES ng acct id

	if(isset($_FILES["profpic"])){
		$directory = "profpics/";
		$validFileTypes = array("jpg","jpeg","png","gif");
		$filename = $directory.$acctid.date("ymdLHis");

		$imageFileType = strtolower(pathinfo(basename($_FILES["profpic"]["name"]),PATHINFO_EXTENSION));
		if(!in_array($imageFileType, $validFileTypes)){
			echo "Not a valid picture";
		}
		elseif($_FILES["profpic"]["size"] > 500000){
			echo "Image is too big";
		}
		else{
			move_uploaded_file($_FILES["profpic"]["tmp_name"], "$filename.$imageFileType");
			$completeFilename = explode("/", $filename);
			$truefilename = $completeFilename[sizeof($completeFilename)-1].'.'.$imageFileType;

			if(!insertProfPic($mysqliconn,$acctid,$truefilename)){
				echo "Something went wrong with the upload";
			}
		}
	}
	if(isset($acctid)){
		$acctinfo = getAccountInfo($mysqliconn,$acctid);
	}
		
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>DB Exercise Profile</title>
</head>
<body>
	<div>
		<form action="logout.php">
			<input type="submit" name="logout" value="Logout">
			<label>Currently Logged in user: <?php echo $acctinfo['username']; ?> </label>
		</form>
		
	</div>
	<h1>Profile Page</h1>
	<img src="<?php if(isset($acctinfo['file_name'])) echo 'profpics/'.$acctinfo['file_name'] ?>" alt="Profile Picture" width="150px" height="150px">
	<form action="" method="post" enctype="multipart/form-data">
		<input type="file" name="profpic" required><input type="submit" name="submit" value="Upload">
	</form>
	<p> <label>Name: </label> <?php echo $acctinfo['first_name']." ".$acctinfo['last_name']; ?> </p>
	<p> <label>Email: </label> <?php echo $acctinfo['email']; ?> </p>
	<p> <label>Birthday: </label> <?php echo $acctinfo['bday']; ?> </p>
	<p> <label>Contact Number: </label> <?php echo $acctinfo['contactno']; ?> </p>


</body>
</html>