<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 7</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 7) Retrieve department name and number of employee in each department. </h2>

	<?php 
		$query = "SELECT `Name` AS 'Department Name', COUNT(`name`) AS 'Number of Employees' FROM `employees` LEFT JOIN `departments` AS `dept` ON `department_id`=`dept`.`id` GROUP BY `name`;";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>