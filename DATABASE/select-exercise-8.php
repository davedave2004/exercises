<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 8</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 8) Retrieve employee's full name and hire date who was hired the most recently.</h2>

	<?php 
		$query = "SELECT CONCAT(`first_name`,' ',`last_name`) AS 'FULL NAME',`hire_date` FROM `employees` WHERE `hire_date`=(SELECT MAX(`hire_date`) FROM `employees`);";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>