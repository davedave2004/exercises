<?php
	include('dbfunctions.php');

	if(isset($_POST) && sizeof($_POST) == 9) {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$confpass = $_POST['confpass'];
		$email = $_POST['email'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$bday = $_POST['bday'];
		$contactno = $_POST['contactno'];

		$proceedToRegister = true;

		//check if username is taken, query ka dito
		if(checkDataIfTaken($mysqliconn,$username,"username")){
				echo "Username already taken <br>";
				$proceedToRegister = false;
		}
		//check if password matches
		if($password !== $confpass){
			echo "Password does not match <br>";
			$proceedToRegister = false;
		}
		//check if email is already taken
		if(checkDataIfTaken($mysqliconn,$email,"email")){
			echo "Email is already in use <br>";
			$proceedToRegister = false;
		}
		//check contactno is taken
		if(checkDataIfTaken($mysqliconn,$contactno,"contactno")){
			echo "Contact number is already in use <br>";
			$proceedToRegister = false;
		}
		if($proceedToRegister){
			insertInfo($mysqliconn,$_POST);
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>DB Exercise: Register</title>
	<style type="text/css">
		.registerform {
			width: 540px;
			height: 500px;
		}
		label {
			display: block;
			width: 540px;
		}
		input {
			width: 200px;
		}
	</style>
</head>
<body>

	<h1><a href="index.php">&laquo; </a> The Registration</h1>

	<div>
		<form class="registerform" method="post" action="" onsubmit="return validateForm();">
			<p>
				<label>Username: </label>
				<input type="text" id="username" name="username" placeholder="Username" required>
			</p>
			<p>
				<label>Password: </label>
				<input type="password" id="password" placeholder="Password" name="password" required>
			</p>
			<p>
				<label>Confirm Password: </label>
				<input type="password" id="confpass" placeholder="password" name="confpass" required>
			</p>
			<p>
				<label>Email Address: </label>
				<input type="text" id="email" name="email" placeholder="juandelacruz@gmail.com" pattern="[A-Za-z0-9.]*@[A-Za-z]*[.].*" required>
			</p>
			<p>
				<label>First Name: </label>
				<input type="text" id="firstname" name="firstname" placeholder="Juan" pattern="[A-Za-z ]*" required>
			</p>
			<p>
				<label>Last Name: </label>
				<input type="text" id="lastname" name="lastname" placeholder="Dela Cruz" pattern="[A-Za-z ]*" required>
			</p>
			<p>
				<label>Birthday: </label>
				<input type="date" id="bday" name="bday" max="<?php echo date('Y-m-d');?>" required>
			</p>
			<p><label>Contact Number: </label><input type="text" pattern="[0-9]{7,11}" maxlength="11" name="contactno" placeholder="Mobile or Phone Number" required></p>
			<input type="submit" id="submit" name="submit" value="Register">
		</form>

		<script type="text/javascript">

			function validateForm(){
				const pass = document.getElementById("password");
				const confpass = document.getElementById("confpass");

				if(pass.value !== confpass.value){
					alert("Password does not match");
					return false;
				}

				return true;
			}
		</script>
	</div>
</body>
</html>