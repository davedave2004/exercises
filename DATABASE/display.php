<?php
	include('dbfunctions.php');
	//FETCH ALL ACCOUNTS
	$allInfo = getAllAccountInfo($mysqliconn);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>DB Exercise: Display</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h1><a href="index.php">&laquo; </a> All Registered Info</h1>
	<table>
		<tr>
			<th>ID</th><th>Username</th><th>Password</th><th>Email</th><th>First Name</th><th>Last Name</th><th>Birthday</th><th>Contact Number</th>
		</tr>
		<?php
			while($row=$allInfo->fetch_assoc()){
				echo "<tr>";
				foreach ($row as $key => $value) {

					if($key == "file_name"){
						echo '<td><img width="150px" height="150px" src="profpics/';
						echo $value;
						echo '" alt="Profile Picture"></td>';
					}	
					else{
						echo "<td>";
						echo $value;
						echo "</td>";
					}
				}
				echo "</tr>";
			}
		?>
	</table>
</body>
</html>