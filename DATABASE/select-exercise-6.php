<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 6</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 6) Retrieve number of employee who has middle name.</h2>

	<?php 
		$query = "SELECT COUNT(*) FROM `employees` WHERE `middle_name` IS NOT NULL AND `middle_name` <> \"\";";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>