<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 1</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 1) Retrieve employees whose last name start with "K".</h2>
	<?php 
		$query = "SELECT * FROM `employees` WHERE `last_name` LIKE 'K%';";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>