<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 3</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.</h2>

	<?php 
		$query = "SELECT CONCAT(`first_name`,' ',`last_name`) AS 'FULL NAME',`hire_date` FROM `employees` WHERE `hire_date` BETWEEN '2015/1/1' AND '2015/3/31';";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>