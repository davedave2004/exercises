<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 10</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 10) Retrieve employee's full name who has more than 2 positions.</h2>

	<?php 
		$query = "SELECT CONCAT(`first_name`,' ',`last_name`) AS 'FULL NAME' FROM `employees` INNER JOIN `employee_positions` ON `employee_id` = `employees`.`id` GROUP BY `FULL NAME` HAVING COUNT(*) >2;";

		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>