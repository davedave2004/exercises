<?php include('dbfunctions.php') ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>SELECT EXERCISE 5</title>
	<link rel="stylesheet" type="text/css" href="table.css">
</head>
<body>
	<h2>Exercise 5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</h2>

	<?php 
		$query = "SELECT `last_name` FROM `employees` WHERE `department_id`=3 ORDER BY `last_name` DESC;";
		echo "<p><strong>Query: </strong> $query </p>";
		queryThenDisplay($mysqliconn,$query);
	?>


</body>
</html>