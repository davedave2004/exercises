<?php
	if(isset($_POST['month']) && isset($_POST['year'])){
		//View the calendar for the given month and year
		$currentMonth = $_POST['month'];
		$currentYear = $_POST['year'];
	}else{
		//View current Date
		$currentMonth = date("F");
		$currentYear = date("Y");

	}


	//Get The Starting Date
	$unixTimeOfstartOfTheMonth=strtotime("1 $currentMonth $currentYear"); //Get the Start of the month


	//Get next and previous month
	$previous = date("F/Y",strtotime("-1 month",$unixTimeOfstartOfTheMonth));
	$next = date("F/Y",strtotime("+1 month",$unixTimeOfstartOfTheMonth));

	$previousMonthYear = explode("/", $previous);
	$nextMonthYear = explode("/", $next);

	$numberOfDaysCurrentMonth = date("t",$unixTimeOfstartOfTheMonth); //Get number of days of current month
	$startDay = date("D",$unixTimeOfstartOfTheMonth); //Get the day of the 1st day of the month
	//Ilagay ung dates dito;
	switch($startDay){
		case "Sun": $startIndex = 1;break;
		case "Mon": $startIndex = 2;break;
		case "Tue": $startIndex = 3;break;
		case "Wed": $startIndex = 4;break;
		case "Thu": $startIndex = 5;break;
		case "Fri": $startIndex = 6;break;
		case "Sat": $startIndex = 7;break;
		default: $startIndex = 0;
	}
	//Ready dates arrays
	$calendardates=[
		"first"=>array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>''),
		"second"=>array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>''),
		"third"=>array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>''),
		"fourth"=>array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>''),
		"fifth"=>array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'')];

	//Populate Date Arrays
	$day = 1;
	$additionalDays = array(); //Container for row and index of additional days;
	$daysHasBeenReset = false;

	foreach ($calendardates as $key => $value) {

		foreach ($value as $index => $dontneed) {	
			if(strcmp($key,"first")==0){
				if($index<$startIndex){
					$minusDays = $startIndex-$index;
					$addtlDay = date("j",strtotime("-$minusDays day",$unixTimeOfstartOfTheMonth));
					$calendardates[$key][$index] = $addtlDay;
					$additionalDays[$key][$index] = 1;
					continue;
				}
			}
			$calendardates[$key][$index] = $day;



			if($daysHasBeenReset) {
				$additionalDays[$key][$index] = 1;
			}

			//Check if 4 rows are already enough for the dates like February 2009
			if((strcmp($key, "fourth")==0) && ($day==$numberOfDaysCurrentMonth) && ($index==7) ){
				array_pop($calendardates);
				break;
			}

			$day++;
			if($day>$numberOfDaysCurrentMonth){
				$day=1; //reset the day
				$daysHasBeenReset=true;
				continue;
			}
			if((strcmp($key, "fifth")==0) && ($index==7) && $day>8 ){
				//if you are in the fifth row, and already reached last BUT day is still less than number of days
				$calendardates['sixth'] = array(1=>'',2=>'',3=>'',4=>'',5=>'',6=>'',7=>'');
			}

		}
		if(!array_key_exists("fifth", $calendardates)) break;
	} //

	if(array_key_exists("sixth", $calendardates)){
		foreach ($calendardates['sixth'] as $index => $dontneed) {
			$calendardates['sixth'][$index] = $day;

			if($daysHasBeenReset) {
				$additionalDays['sixth'][$index] = 1;
			}

			$day++;
			if($day>$numberOfDaysCurrentMonth){
				$day=1; //reset the day
				$daysHasBeenReset=true;
				continue;
			}
		}
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Calendar Exercise</title>
	<style type="text/css">

		table {
			border-collapse: collapse;
			width: 100%;

		}
		table, th, td {
			border: 1px solid black;
		}

		th, td {
			padding: 15px;
			text-align: left;
		}
		td{
			text-align: right;
		}
		.header{
			width: 100%;
			display: flex;
			margin-bottom: 10px;
		}
		h1 {
			flex-grow: 4;
			text-align: center;
		}
		form {
			flex-grow: 1;
			display: flex;
		}
		input[type=submit] {
			flex-grow: 1;
		}
		.additionalday {
			background-color: WhiteSmoke;
		}

	</style>
</head>
<body>
	<div class="header">
		<form method="post" action="">
			<input type="hidden" name="month" value="<?php echo $previousMonthYear[0]?>">
			<input type="hidden" name="year" value="<?php echo $previousMonthYear[1]?>">
			<input type="submit" name="submit" value="&laquo;">
		</form>
		<h1><?php echo "$currentMonth $currentYear";?></h1>
		<form method="post" action="">
			<input type="hidden" name="month" value="<?php echo $nextMonthYear[0]?>">
			<input type="hidden" name="year" value="<?php echo $nextMonthYear[1]?>">
			<input type="submit" name="submit" value="&raquo;">
		</form>
	</div>
	<table>
		<tr>
			<th>Sunday</th>
			<th>Monday</th>
			<th>Tuesday</th>
			<th>Wednesday</th>
			<th>Thursday</th>
			<th>Friday</th>
			<th>Saturday</th>
		</tr>

		<?php foreach ($calendardates as $row => $dates): ?>
			<tr>
				<?php foreach ($dates as $index => $day):
					if(isset($additionalDays[$row][$index])):
				?>
					<td class="additionalday">
						<?php echo $day?>		
					</td>
				<?php else: ?>
					<td>
						<?php echo $day?>		
					</td>
				<?php endif;endforeach; ?>
			</tr>	
		<?php endforeach; ?>
	</table>

</body>
</html>