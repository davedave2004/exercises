<?php
use PHPMailer\PHPMailer\PHPMailer;

	if(isset($_POST['email'])&&isset($_POST['receiver'])&&isset($_POST['emailbody'])){
		
		require_once ("vendor/autoload.php");

		$mail = new PHPMailer;

		$sender = $_POST['email'];
		$receiver = $_POST['receiver'];
		$emailbody = $_POST['emailbody'];

		$mail->From = $sender;
		$mail->FromName = $_POST['name'];
		$mail->addAddress($receiver);
		$mail->addReplyTo($sender);
		$mail->Subject = $_POST['subject'];
		$mail->Body = $emailbody;
		$mail->AltBody =  $emailbody;

		if(!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		else 
		{
		    echo "Your message has been sent successfully";

		    //Send a message to sender that sending was a success;
		   	$mail->ClearAddresses();
   			$mail->From = "Localhost@localhost.com";
			$mail->FromName = "Server";
			$mail->AddAddress($sender);
			$mail->Body = "Thank you for using mail form, your email to $receiver was sent.";

			if(!$mail->send()){
			    echo "Mailer Error: " . $mail->ErrorInfo;
			}else echo "We also sent you a message";
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Mail Form</title>
	<style type="text/css">
		form {
			display: flex;
			flex-flow: column;
		}
	</style>
</head>
<body>
	<h1><center>Mail form</center></h1>
	<form id="mailform" method="post" action="" onsubmit="return confirm('Send mail using gmail?');">
		<label>Email</label><input type="email" name="email" required>
		<label>Name</label><input type="text" name="name">
		<label>Send To</label><input type="email" name="receiver" required>
		<label>Subject</label><input type="text" name="subject">
		<label>Body</label><textarea form="mailform" name="emailbody" rows="8"></textarea>
		<input type="submit" name="submit" value="Send Mail">
	</form>

</body>
</html>