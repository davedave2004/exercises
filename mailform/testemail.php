<?php
	use PHPMailer\PHPMailer\PHPMailer;

	if(isset($_POST['email'])&&isset($_POST['pass'])&&isset($_POST['emailbody'])){
		
		require_once ("vendor/autoload.php");

		$mail = new PHPMailer;

		//Enable SMTP debugging. 
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name                          
		$mail->Host = "smtp.gmail.com";
		//Set this to true if SMTP host requires authentication to send email
		$mail->SMTPAuth = true;                          
		//If SMTP requires TLS encryption then set it
		$mail->SMTPSecure = "tls";                           
		//Set TCP port to connect to 
		$mail->Port = 587;                                   


		//Provide username and password     
		$mail->Username = $_POST['email'];                 
		$mail->Password = $_POST['pass'];
		$mail->From = $_POST['email'];
		$mail->FromName = $_POST['name'];
		$mail->addAddress($_POST['receiver']);
		//$mail->isHTML(true);
		$emailbody = $_POST['emailbody'];
		$mail->Subject = $_POST['subject'];
		$mail->Body = $emailbody;
		$mail->AltBody =  $emailbody;

		if(!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;

		} 
		else 
		{
		    echo "Message has been sent successfully";
		   	$mail->ClearAddresses();

			$mail->AddAddress($sender);
			$mail->Body = "Thank you for using mail form, your email to $receiver was sent.";

			if(!$mail->send()){
			    echo "Mailer Error: " . $mail->ErrorInfo;
			}else echo "We also sent you a message";		    
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Mail Form</title>
	<style type="text/css">
		form {
			display: flex;
			flex-flow: column;
		}
	</style>
</head>
<body>
	<h1><center>Mail form using GMAIL</center></h1>
	<form id="mailform" method="post" action="" onsubmit="return confirm('Send mail using gmail?');">
		<label>GMAIL</label><input type="email" name="email" required>
		<label>Password</label><input type="password" name="pass" required>
		<label>Name</label><input type="text" name="name">
		<label>Send To</label><input type="email" name="receiver">
		<label>Subject</label><input type="text" name="subject">
		<label>Body</label><textarea form="mailform" name="emailbody" rows="8"></textarea>
		<input type="submit" name="submit" value="Send Mail">
	</form>

</body>
</html>