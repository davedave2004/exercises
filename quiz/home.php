<?php 
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		$acctid = $_SESSION['acctid'];
	}else header("Location: index.php");
	//FETCH LAHAT NG VALUES ng acct id
	if(isset($acctid)){
		$fullname = getFullName($mysqliconn,$acctid);
		//Exams taken and average
		$avg = 0;
		$grades = getExistingGrades($mysqliconn,$acctid);
		$examstaken = $grades->num_rows;
		while($grade = $grades->fetch_assoc()){
			$avg += $grade['grade'];
		}
		$avg = $avg/$examstaken;

		//GET EXAM LIST
		$allExams = getAllExams($mysqliconn);
		//GET EXAM SUMMARY
	}		
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tesuto</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<input type="hidden" id="acctid" value="<?php echo $acctid;?>">
	<div class="header">
		<a href="home.php"><img class="logo" src="tesuto.png" alt="Tesuto Logo"></a>
		<span class="header-text"><?php echo $fullname?></span>
		<span class="right-header">
					Exams Taken: <?php echo $examstaken; ?> | Average Grade: <?php echo $avg; ?> |	<a href="summary.php">View Summary</a> |
					<form onsubmit="return confirm('Logout?');" action="logout.php"><input type="submit" name="logout" value="Logout"></form>
		</span>
	</div>
<!--
	<div class="search-sort-bar">
		<div class="sort-bar">
			<label class="sort">Sort By: </label>
			<select>
				<option value="category">Category</option>
				<option value="name">Name</option>
				<option value="difficulty">Difficulty</option>
			</select>
		</div>
	</div>
-->
	<div class="exams-container">
		<div class="exam-list">
			<table>
				<tr>
					<?php while($exam = $allExams->fetch_array(MYSQLI_ASSOC)):
					?><td>
						<img class="table-exam-icon"src="<?php echo $exam['image']?>" alt="Exam Icon" width="150px" height="150">
						<span data-examid="<?php echo $exam['id']?>" class="table-exam-name"><?php echo $exam['name']?></span>
					</td>
					<?php
						endwhile;
					?>
				</tr>
			</table>
		</div>
	</div>

	<div id="exam-display" class="exam-display">
		<img id="exam-image" class="exam-image" src="" alt="" width="200px" height="300px">
		<div class="exam-info">
			<p>Exam Name:<span id="exam-info-name"></span></p>
			<p>Difficulty: <span id="exam-info-diff"></span></p>
			<p>Category: <span id="exam-info-categ"></span></p>
			<p>Last Grade: <span id="exam-info-lastgrade"></span></p>
			<p>Last Taken on: <span id="exam-info-lasttake"></span></p>
		</div>
		<form class="exam-take" action="exam.php" method="post">
			<input id="hiddenid" type="hidden" name="examnumber" value="">
			<input type="submit" name="submit" value="Take Exam">
		</form>
	</div>

	<script type="text/javascript">
		const acctid = document.getElementById("acctid").value;
		const examDisplay = document.getElementById("exam-display");
		const exams = document.querySelectorAll('td');
		const examInfoImage = document.getElementById("exam-image");
		const examInfoName = document.getElementById("exam-info-name");
		const examInfoCateg = document.getElementById("exam-info-categ");
		const examInfoDiff = document.getElementById("exam-info-diff");
		const examInfoLastGrade = document.getElementById("exam-info-lastgrade");
		const examInfoLastTake = document.getElementById("exam-info-lasttake");
		const hiddenIdInput = document.getElementById("hiddenid");
		const rightscroll= document.getElementById("right-scroll");
		const leftscroll= document.getElementById("left-scroll");

		const examlist = document.querySelector(".exam-list");
		const maxscroll = examlist.scrollWidth-examlist.clientWidth;

		exams.forEach(exam => exam.addEventListener('click',function(e){
			const examImg = this.querySelector('img');
			const examNameSpan = this.querySelector('span');
			const examId = examNameSpan.dataset.examid;

			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					const res = JSON.parse(this.responseText);
					examDisplay.style.visibility = "visible";
					examInfoImage.src = examImg.src;
					examInfoName.innerHTML=res.examname;
					examInfoCateg.innerHTML = res.category;
					examInfoDiff.innerHTML = res.difficulty;

					(res.grade != "None") ? examInfoLastGrade.innerHTML = `${res.grade}%` : examInfoLastGrade.innerHTML =  "None";
					
					examInfoLastTake.innerHTML = res.last_updated;
					hiddenIdInput.value=examId;

				}
			};
			xhttp.open("GET", `dbfunctions.php?exno=${examId}&userid=${acctid}`, true);
			xhttp.send();
		}));		

	</script>
</body>
</html>