<?php 
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		$acctid = $_SESSION['acctid'];
	}else header("Location: index.php");
	//FETCH LAHAT NG VALUES ng acct id
	if(isset($acctid)){
		$fullname = getFullName($mysqliconn,$acctid);

		$avg = 0;
		$grades = getExistingGrades($mysqliconn,$acctid);
		$examstaken = $grades->num_rows;
		while($grade = $grades->fetch_assoc()){
			$avg += $grade['grade'];
		}
		$avg = $avg/$examstaken;

		$grades->data_seek(0);
	}
		
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tesuto Summary</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<a href="home.php"><img class="logo" src="tesuto.png" alt="Tesuto Logo"></a>
		<span class="header-text"><?php echo $fullname ?></span>
		<span class="right-header">
					Exams Taken: <?php echo $examstaken; ?> | Average Grade: <?php echo $avg; ?> |	<a href="summary.php">View Summary</a> |
					<form action="logout.php"><input type="submit" name="logout" value="Logout"></form>
		</span>
	</div>
	<div>
		<center><h1>EXAM GRADES</h1></center>
	</div>
	<table class="summary-table">
		<tr>
			<th class="summary-th">Exam Name</th>
			<th class="summary-th">Grade</th>
			<th class="summary-th">Date last taken</th>

			<?php
				while ($grade = $grades->fetch_assoc()):
			?>
			<tr class="summary-tr">
			<td class="summary-td"><?php echo $grade['examname']; ?></td>
			<td class="summary-td"><?php echo $grade['grade']; ?></td>
			<td class="summary-td"><?php echo $grade['last_updated']; ?></td>
			</tr>
			<?php
				endwhile;
			?>


		</tr>
	</table>

	<?php
		$examsnottaken = 10-$examstaken;
		if($examsnottaken != 0) {
			echo "<center>There are still ".$examsnottaken." exams for you</center>";
		}
	?>

</body>
</html>