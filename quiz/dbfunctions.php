<?php
	include('db.php');
	//Check if account and password matches in database
	function getLogin($conn,$data){
		//Check first if username is registered
		if(checkDataIfTaken($conn,$data['username'],"username")){
			//Check if username and password matches the data in db
			$selectquery = "SELECT `id`,`password` FROM users WHERE username=?";
			if($prepSelect = $conn->prepare("$selectquery")){
				$prepSelect->bind_param("s",$data['username']);
				$prepSelect->execute();
				$result = $prepSelect->get_result();
				$prepSelect->close();
				if($result->num_rows>0){
					$row = $result->fetch_array(MYSQLI_ASSOC);
					$checkpass = $row['password'];
					if(password_verify($data['password'],$checkpass)){
						return	$row['id'];
					} else echo "Wrong account or password"; 
				}
			}
		} else echo "That user is not registered";
		return false;
	}

	//Check username if taken, return true or false
	function checkDataIfTaken($conn,$data){
		$selectquery = "SELECT `username` FROM users WHERE `username`=?";

		if($prepSelect = $conn->prepare($selectquery)){
			$prepSelect->bind_param('s',$data);
			$prepSelect->execute();
			$result = $prepSelect->get_result();
			$prepSelect->close();
			return ($result->num_rows >0);
		} else echo $conn->error;
	}

	function getFullName($conn,$acctid){
		$selectquery = "SELECT CONCAT(`first_name`,' ',`last_name`) AS 'fullname' FROM users WHERE `id`=?";

		if($preparedStmt = $conn->prepare($selectquery)){
			$preparedStmt->bind_param('i',$acctid);
			$preparedStmt->execute();
			$result = $preparedStmt->get_result();
			$preparedStmt->close();
			$array = $result->fetch_array(MYSQLI_ASSOC);
			return $array['fullname'];
		}
	}

	function insertInfo($conn,$userInfo){
		$insertquery = "INSERT INTO users (username,password,first_name,last_name) VALUES (?,?,?,?)";
		$hashedpass = password_hash($userInfo['password'],PASSWORD_DEFAULT);
		if($prepInsert = $conn->prepare($insertquery)){
			$prepInsert->bind_param("ssss",$userInfo['username'],$hashedpass,$userInfo['firstname'],$userInfo['lastname']);
			if($prepInsert->execute()){
				echo "You are now registered!";
			} else echo $conn->error;
		} else echo $prepInsert->error;
		$prepInsert->close();
	}

	function getAllExams($conn){
		$selectAll = "SELECT `a`.`id`,`a`.`name`,`b`.`category`,`difficulty`,`image` FROM `exams` as `a` INNER JOIN `categories` as `b` ON `a`.`category_id`=`b`.`id`;";

		if($stmt = $conn->prepare($selectAll)){
			$stmt->execute();
			$result = $stmt->get_result();
			$stmt->close();
			return $result;
		}else echo "Something went wrong ".$conn->error;

	}

	function getExamQuestions($conn,$examid){
		$selectExamQuestions = "SELECT `a`.`name`,`b`.`id` AS 'id',`question` as 'text' FROM `exams` AS `a` INNER JOIN `questions` as `b` ON `a`.`id` = `b`.`exam_id` WHERE `a`.`id`=?";

		if($stmt = $conn->prepare($selectExamQuestions)){
			$stmt->bind_param("i",$examid);
			$stmt->execute();
			$questions = $stmt->get_result();
			$stmt->close();

			$answers = getAnswersForExam($conn,$examid);

			return array($questions,$answers);
		}else echo "Could not get the exam".$conn->error;
	}

	function getAnswersForExam($conn,$examid){
		$selectAnswers = "SELECT * FROM `answers` WHERE `question_id` IN (SELECT `id` FROM `questions` WHERE `exam_id`=?)";
		if($stmt = $conn->prepare($selectAnswers)){
			$stmt->bind_param("i",$examid);
			$stmt->execute();
			$answers = $stmt->get_result();
			$stmt->close();
			return $answers;
		}else echo "Could not get the answers".$conn->error;
	}

	//GET Exam info, previous grade, question id for that exam
	function getExamInfoWithPreviousGrade($conn,$examid,$acctid){
		$selectExamInfo = "SELECT `name` as 'examname',(SELECT `category` FROM `categories` WHERE `categories`.`id`=`category_id`) AS 'category',`difficulty` FROM `exams` WHERE  `exams`.`id`=?"; //examname

//		$selectExamInfo = "SELECT `name` as 'examname',(SELECT `category` FROM `categories` WHERE `categories`.`id`=`category_id`) AS 'category',`difficulty` FROM `exams` WHERE  `exams`.`id`=?"; //examname,category,difficulty
		if($stmt = $conn->prepare($selectExamInfo)){
			$stmt->bind_param("i",$examid);
			$stmt->execute();

			$result = $stmt->get_result();
			$info = $result->fetch_assoc();
			$result->free();
			$stmt->close();

			$grades = getPreviousGrade($conn,$examid,$acctid);
			return array_merge($info,$grades);

		}else echo "Cannot get the information the moment. ".$conn->error;
		//result
	}

	function getPreviousGrade($conn,$examid,$acctid){
		$selcetPreviousGrade = "SELECT `grade`,`last_updated` FROM `grades` WHERE `exam_id`=? AND `user_id`=?"; //grade,last_updated. pag wala nulls;
		if($stmt = $conn->prepare($selcetPreviousGrade)){
			$stmt->bind_param("ii",$examid,$acctid);
			$stmt->execute();
			$result = $stmt->get_result();

			if($result->num_rows>0){ //means he already has a grade for the exam
				$grades = $result->fetch_assoc();
			}else {
				$grades = array("grade"=>"None","last_updated"=>"Not yet taken");
			}
			$result->free();
			$stmt->close();
			return $grades;
		}else "Cannot get previous grades. ".$conn->error;
		return array("grade"=>"None","last_updated"=>"Not yet taken");
	}

	function getCorrectAnswers($conn,$examid){
		$selectAnswers = "SELECT `id` AS 'question_id',`answer_id` AS 'correct_answer' FROM `questions` WHERE `exam_id`=?;";

		if($stmt = $conn->prepare($selectAnswers)){
			$stmt->bind_param("i",$examid);
			$stmt->execute();
			$result = $stmt->get_result();
			$stmt->close();
			return $result;
		}else echo "Cannot get the answers right now. ".$conn->error;
	}

	function insertGrade($conn,$examid,$acctid,$percentGrade){
		//gawin nating percent ung score
		$inserted = false; 
		$selectGrade = "INSERT INTO `grades`(`exam_id`,`user_id`,`grade`) VALUES (?,?,?)";
		if($stmt = $conn->prepare($selectGrade)){
			$stmt->bind_param('iii',$examid,$acctid,$percentGrade);
			if(	$stmt->execute()){
				$inserted = true;
			}
			$stmt->close();
		}else echo "Cannot Insert Grade. ".$conn->error;
		return $inserted;
	}

	function updateGrade($conn,$examid,$acctid,$percentGrade){
		$updated = false;
		
		$updateGrade = "UPDATE `grades` SET `grade`=? WHERE `exam_id`=? AND `user_id`=?";
		if($stmt = $conn->prepare($updateGrade)){
			$stmt->bind_param("iii",$percentGrade,$examid,$acctid);
			if($stmt->execute()){
				$updated = true;
			}
			$stmt->close();
		}else echo "Cannnot Update Grade. ".$conn->error;
		return $updated;
	}

	function getExistingGrades($conn,$acctid){
		$selectGrades = "SELECT `b`.`name` as 'examname',(SELECT `category` FROM `categories` WHERE `categories`.`id`=`b`.`category_id`) AS 'category',`difficulty`,`last_updated`,`grade` FROM `grades` as `a` INNER JOIN `exams` as `b` ON `a`.`exam_id`=`b`.`id` WHERE `user_id` = ?;";

		if($stmt = $conn->prepare($selectGrades)){
			$stmt->bind_param("i",$acctid);
			$stmt->execute();
			$result = $stmt->get_result();
			$stmt->close();
			return $result;
		}else echo "Could not get grades. ".$conn->error;
	}

	//For ajax purposes hehe
	if(isset($_GET['exno'])&&isset($_GET['userid'])){
		$examnumber = $_GET['exno'];
		$acctid = $_GET['userid'];

		$info = getExamInfoWithPreviousGrade($mysqliconn,$examnumber,$acctid);
		echo json_encode($info);
	}


?>