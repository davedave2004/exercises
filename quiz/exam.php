<?php 
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		$acctid = $_SESSION['acctid'];
	}else header("Location: index.php");
	//FETCH LAHAT NG VALUES ng acct id
	if(isset($acctid)){
		$fullname = getFullName($mysqliconn,$acctid);
		//Exams taken and average
		$avg = 0;
		$grades = getExistingGrades($mysqliconn,$acctid);
		$examstaken = $grades->num_rows;
		while($grade = $grades->fetch_assoc()){
			$avg += $grade['grade'];
		}
		$avg = $avg/$examstaken;

	}
	if(!isset($_POST['examnumber'])){
		header("Location: home.php");
	}else {
		$examnumber = $_POST['examnumber'];
		$result = getExamQuestions($mysqliconn,$examnumber);
		$allQuestions = $result[0];
		$allAnswers = $result[1];
		$firstrow = $allQuestions->fetch_row();
		$examname = $firstrow[0];
		$allQuestions->data_seek(0);
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tesuto: <?php echo $examname ?></title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<a href="home.php"><img class="logo" src="tesuto.png" alt="Tesuto Logo"></a>
		<span class="header-text"><?php echo $fullname?></span>
		<span class="right-header">
					Exams Taken: <?php echo $examstaken; ?> | Average Grade: <?php echo $avg; ?> |	<a href="summary.php">View Summary</a> |
					<form onsubmit="return confirm('Logout?');" action="logout.php"><input type="submit" name="logout" value="Logout"></form>
		</span>
	</div>

	<h2><?php echo $examname ?></h2>
	<form method="post" action="result.php" onsubmit="return confirm('Submit Exam?');">
		<input type="hidden" name="examnumber" value="<?php echo $examnumber; ?>">
		<div class="question-list">
			<?php
				while($question = $allQuestions->fetch_array(MYSQLI_ASSOC)):
			?>

			<div class="question">
				<p class="question-text"><?php echo $question['text'] ?></p>
				<?php 
					for($i=0;$i<3;$i++):
						$answer = $allAnswers->fetch_array(MYSQLI_ASSOC);
				?>
					<input type="radio" name="<?php echo $answer['question_id'] ?>" value="<?php echo $answer['id']?>" required>
					<?php echo $answer['answer'] ?><br>
	 
	  			<?php
	  				endfor;
	  			?>
			</div>

			<?php
				endwhile;
			?>
			<input type="submit" name="submit" value="Submit Exam">
		</div>
	</form>
</body>
</html>