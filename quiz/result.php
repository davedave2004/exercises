<?php 
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		$acctid = $_SESSION['acctid'];
	}else header("Location: index.php");
	//FETCH LAHAT NG VALUES ng acct id
	if(isset($acctid)){
		$fullname = getFullName($mysqliconn,$acctid);
		//Exams taken and average
		$avg = 0;
		$grades = getExistingGrades($mysqliconn,$acctid);
		$examstaken = $grades->num_rows;
		while($grade = $grades->fetch_assoc()){
			$avg += $grade['grade'];
		}
		$avg = $avg/$examstaken;

	}
	if(isset($_POST['examnumber'])){
		$examnumber = $_POST['examnumber'];

		$examInfoWithGrade = getExamInfoWithPreviousGrade($mysqliconn,$examnumber,$acctid);
		$previousGrade = $examInfoWithGrade['grade'];
		//GET question id and ANSWERS FOR this EXAM ID
		$correct_answers = getCorrectAnswers($mysqliconn,$examnumber);
		//CHECK ANSWERS IN POST DATA
		$given_answers = array_slice($_POST,1,10,true); //Array of answers with key of questionid

		$score = 0;
		while ($correctanswer = $correct_answers->fetch_assoc()) {
			$questionid = $correctanswer['question_id'];
			$correct = $correctanswer['correct_answer'];
			$givenanswer = $given_answers[$questionid];
			if($correct == $givenanswer){
				$score++;
			}
		}
		$percentGrade = $score*10;
		if(strcmp($previousGrade,"None")==0){//None then insert grade, else update
			if(!insertGrade($mysqliconn,$examnumber,$acctid,$percentGrade)) echo "Something went wrong";

			$comparison = "This is your first take!";

		}else{
			if(!updateGrade($mysqliconn,$examnumber,$acctid,$percentGrade)) echo "Something went wrong";

			if($previousGrade>$percentGrade) {
				$comparison = "You can do better than that!";
			}
			elseif($previousGrade<$percentGrade) {
				$comparison = "Wow, there was an improvement!";
			}elseif($previousGrade==$percentGrade) {
				$comparison = "Your result is the same as last";
			}
		}

		if($percentGrade==100){
			$finalmessage = "Wow! You got the highest mark possible!";
		}elseif( ($percentGrade<100) && ($percentGrade>50) ){
			$finalmessage = "Congratulations, you passed!";
		}elseif( $percentGrade == 50) {
			$finalmessage = "You did well";
		}else {$finalmessage = "What happened? You could use a little review on that subject";}
	}
		
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tesuto: Result</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<a href="home.php"><img class="logo" src="tesuto.png" alt="Tesuto Logo"></a>
		<span class="header-text"><?php echo $fullname?></span>
		<span class="right-header">
					Exams Taken: <?php echo $examstaken; ?> | Average Grade: <?php echo $avg; ?>|	<a href="summary.php">View Summary</a> |
					<form onsubmit="return confirm('Logout?');" action="logout.php"><input type="submit" name="logout" value="Logout"></form>
		</span>
	</div>

	<div class="result">
		<h2 class="result-header">Result for <?php echo $examInfoWithGrade['examname'] ?></h2>
		<div class="result-set">
			<div class="result-text">

				<p>Score: <?php echo "$score/10";?></p>
				<p>Equivalent Grade: <?php echo $score*10 ?>%</p>
				<p><?php echo $comparison;?></p>
				<p>Last grade was: <?php echo $previousGrade; if(strcmp($previousGrade,"None")!=0) echo "%";?></p>
				<p>Last take was on: <?php echo $examInfoWithGrade['last_updated'];?></p>
			</div>
			<div class="result-options">
				<p><?php echo $finalmessage ?></p>
				<form method="post" action="exam.php">
					<input id="hiddenid" type="hidden" name="examnumber" value="<?php echo $examnumber?>">
					<input type="submit" name="submit" value="Take Exam Again">
				</form>
				<form method="post" action="home.php">
					<input type="submit" name="submit" value="Choose another Exam">
				</form>
				
			</div>
		</div>

	</div>
</body>
</html>