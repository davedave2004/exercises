<?php
	session_start();
	include('dbfunctions.php');

	if(isset($_SESSION['acctid'])){
		header("Location: home.php");
	}

	if(isset($_POST['username']) && isset($_POST['password'])){
		$acctid = getLogin($mysqliconn,$_POST);
		if($acctid){
			$_SESSION['acctid'] = $acctid;
			header("Location: home.php");
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tesuto</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<a href="index.php"><img class="logo" src="tesuto.png" alt="Tesuto Logo"></a>
		<span class="tesuto">Tesuto</span>
	</div>

	<div class="login-container">
		<form method="post" action="" >
		<input type="text" name="username" placeholder="Username" required>
		<input type="password" name="password" placeholder="Password" required>
		<div>
			<a href="register.php">Register Here</a>
			<input type="submit" name="submit" value="Submit">
		</div>
	</form>
	</div>

	<script type="text/javascript">
		
	</script>
</body>
</html>