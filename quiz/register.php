<?php
	include('dbfunctions.php');

	//Check if post data is available and correct length
	if(isset($_POST) && sizeof($_POST) == 6) {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$confpass = $_POST['confpass'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];

		$proceedToRegister = true;

		//check if username is taken, query ka dito
		if(checkDataIfTaken($mysqliconn,$username)){
				echo "Username already taken <br>";
				$proceedToRegister = false;
		}
		//check if password matches
		if($password !== $confpass){
			echo "Password does not match <br>";
			$proceedToRegister = false;
		}

		if($proceedToRegister){
			echo "PROCEED TO REGISTER";
			insertInfo($mysqliconn,$_POST);
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Tesuto: Register</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="header">
		<a href="index.php"><img class="logo" src="tesuto.png" alt="Tesuto Logo"></a>
		<span class="tesuto">Tesuto</span>
	</div>
	<div class="register-div">
		<form class="register-form" method="post" action="" onsubmit="return validateForm();">
			<p>
				<label class="register-label">Username: </label>
				<input type="text" id="username" name="username" placeholder="Username" required>
			</p>
			<p>
				<label class="register-label">Password: </label>
				<input type="password" id="password" placeholder="Password" name="password" minlength="8" required>
			</p>
			<p>
				<label class="register-label">Confirm Password: </label>
				<input type="password" id="confpass" placeholder="Confirm Password" name="confpass" minlength="8" required>
			</p>
			<p>
				<label class="register-label">First Name: </label>
				<input type="text" id="firstname" name="firstname" placeholder="Juan" pattern="[A-Za-z ]*" required>
			</p>
			<p>
				<label class="register-label">Last Name: </label>
				<input type="text" id="lastname" name="lastname" placeholder="Dela Cruz" pattern="[A-Za-z ]*" required>
			</p>
			<input type="submit" id="submit" name="submit" value="Register">
		</form>
	</div>

	<script type="text/javascript">

		function validateForm(){
			const pass = document.getElementById("password");
			const confpass = document.getElementById("confpass");

			if(pass.value !== confpass.value){
				alert("Password does not match");
				return false;
			}
			return true;
		}
	</script>

</body>
</html>